# see-me
The see|me app is a one stop directory of resources for the homeless and low-income community.
All too often, looking for resources is a clunky experience with design as an afterthought for
disenfranchised populations. We believe looking for resources should be seamless and that everyone
deserves a beautifully designed experience.