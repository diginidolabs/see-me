/*
 * Copyright 2015 John Krause
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme;

import com.diginido.legacy.appseeme.BuildConfig;

import org.osmdroid.util.GeoPoint;

/**
 * @author John Krause
 * @since 8/6/15
 */
public abstract class Constants {

	// ******************************************
	// Shared Preferences
	// ******************************************
	public static final String PREFERENCE_FIRST_LAUNCH = "firstLaunch";
	public static final GeoPoint DEFAUL_LOCATION = new GeoPoint(37.775, -122.4183333);
	public static final boolean DEBUG = BuildConfig.DEBUG;

	public static String getLogTag(Object context) {
		return String.format("SeeMee-%s", context.getClass().getSimpleName());
	}

}
