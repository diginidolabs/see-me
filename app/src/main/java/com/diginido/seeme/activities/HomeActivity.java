/*
 * Copyright 2015 John Krause
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.diginido.legacy.appseeme.R;
import com.diginido.seeme.model.Section;

public class HomeActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		initViews();
	}

	private void initViews() {
		ImageButton restButton = (ImageButton) findViewById(R.id.rest_icon_button);
		ImageButton eatsButton = (ImageButton) findViewById(R.id.eats_icon_button);
		ImageButton facilitiesButton = (ImageButton) findViewById(R.id.facilities_icon_button);
		ImageButton workButton = (ImageButton) findViewById(R.id.work_icon_button);
		ImageButton healthButton = (ImageButton) findViewById(R.id.health_icon_button);
		ImageButton legalButton = (ImageButton) findViewById(R.id.legal_icon_button);
		restButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startMainActivity(Section.Sections.SHELTER);
			}
		});
		eatsButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startMainActivity(Section.Sections.EATS);
			}
		});
		facilitiesButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startMainActivity(Section.Sections.FACILITIES);
			}
		});
		workButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startMainActivity(Section.Sections.EMPLOYMENT);
			}
		});
		healthButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startMainActivity(Section.Sections.HEALTH);
			}
		});
		legalButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startMainActivity(Section.Sections.LEGAL);
			}
		});
	}

	private void startMainActivity(Section.Sections sectionToShow) {
		Intent intent = new Intent(this, MainActivity.class);
		intent.putExtra(Section.class.getName(), sectionToShow);
		startActivity(intent);
	}

	@Override
	public void onBackPressed() {
		finish();
	}
}
