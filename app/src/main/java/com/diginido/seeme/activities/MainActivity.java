/*
 * Copyright 2015 John Krause
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.diginido.legacy.appseeme.R;
import com.diginido.seeme.fragments.MapFragment;
import com.diginido.seeme.fragments.RadialMenuFragment;
import com.diginido.seeme.fragments.ResultsListFragment;
import com.diginido.seeme.model.Filters;
import com.diginido.seeme.model.Section;
import com.diginido.seeme.network.response.model.Page;
import com.diginido.seeme.network.response.model.SearchResultsMeta;
import com.diginido.seeme.network.service.PageService;
import com.diginido.seeme.network.service.ServiceFactory;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends BaseActivity
		implements ResultsListFragment.OnResultsListActionListener, MapFragment.OnMapActionListener {

	private ResultsListFragment resultsList = new ResultsListFragment();
	private MapFragment mapFragment = new MapFragment();
	private ImageButton radialButton, mapButton;
	private ImageView headerImage;
	private ViewGroup rootLayout, fragmentContainer;
	private View loaderIcon;
	private TextView errorText;
	private final PageService pageService = ServiceFactory.getPageService();
	private Animation loaderAnimation;
	private Section currentSection;
	private long listOrMapViewSectionStart, searchStartTime;
	private boolean hasViewedFirstPage;

	private boolean isMapShowing;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		hasViewedFirstPage = false;
		updateSearchStartTime();
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		initViews();
		handleStartupIntent();
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (currentSection != null
				&& (currentSection.getPages() == null
				|| currentSection.getPages().size() <= 0)) {
			onRefreshList();
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		pageService.cancel(currentSection);
	}

	private void initViews() {
		rootLayout = (ViewGroup) findViewById(R.id.root);
		radialButton = (ImageButton) findViewById(R.id.home_button);
		radialButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				RadialMenuFragment radialMenuFragment = new RadialMenuFragment();
				radialMenuFragment.setOnFinalSelectionListener(new RadialMenuFragment.OnFinalSelectionListener() {
					@Override
					public void onFinalSelectionMade(int sectionMenuId, Filters filters) {
						switch (sectionMenuId) {
							case R.id.rest_icon_button:
								showSection(Section.Sections.SHELTER);
								break;
							case R.id.facilities_icon_button:
								showSection(Section.Sections.FACILITIES);
								break;
							case R.id.legal_icon_button:
								showSection(Section.Sections.LEGAL);
								break;
							case R.id.work_icon_button:
								showSection(Section.Sections.EMPLOYMENT);
								break;
							case R.id.health_icon_button:
								showSection(Section.Sections.HEALTH);
								break;
							case R.id.eats_icon_button:
								showSection(Section.Sections.EATS);
								break;
							default:
								finish();
						}
					}
				});
				radialMenuFragment.show(v, MainActivity.this);
			}
		});
		mapButton = (ImageButton) findViewById(R.id.map_button);
		mapButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				toggleMap();
			}
		});
		headerImage = (ImageView) findViewById(R.id.action_bar_image);
		loaderIcon = findViewById(R.id.loader_icon);
		loaderAnimation = new TranslateAnimation(loaderIcon.getX() - 70, loaderIcon.getX() + 70, loaderIcon.getY(), loaderIcon.getY());
		loaderAnimation.setRepeatCount(Animation.INFINITE);
		loaderAnimation.setRepeatMode(Animation.REVERSE);
		loaderAnimation.setDuration(1000);
		errorText = (TextView) findViewById(R.id.error_text);
		errorText.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onRefreshList();
			}
		});
		fragmentContainer = (ViewGroup) findViewById(R.id.fragment_container);
		resultsList.setActionListener(this);
		mapFragment.setOnMapActionListener(this);
	}

	private void handleStartupIntent() {
		Intent intent = getIntent();
		Section.Sections section = (Section.Sections) intent.getSerializableExtra(Section.class.getName());
		showSection(section);
		showMapFragment();
	}

	private void showSection(Section section) {
		if (currentSection != null) {
			pageService.cancel(currentSection);
		}
		currentSection = section;
		rootLayout.setBackgroundResource(currentSection.getBackground());
		headerImage.setImageResource(currentSection.getHeaderText());
		retrievePages(currentSection);
		updateListOrMapViewTime();
	}

	private void showSection(Section.Sections section) {
		showSection(Section.getSection(section));
	}

	private void toggleMap() {
		if (isMapShowing) {
			showListFragment();
		} else {
			showMapFragment();
		}
		updateListOrMapViewTime();
	}

	private void updateListOrMapViewTime() {
		listOrMapViewSectionStart = System.currentTimeMillis();
	}

	private void updateSearchStartTime() {
		searchStartTime = System.currentTimeMillis();
	}

	private void showListFragment() {
		switchFragment(resultsList);
		isMapShowing = false;
		mapButton.setImageResource(R.drawable.icon_map);
	}

	private void showMapFragment() {
		switchFragment(mapFragment);
		isMapShowing = true;
		mapButton.setImageResource(R.drawable.icon_list);
	}

	private void switchFragment(Fragment fragment) {
		getSupportFragmentManager().beginTransaction()
				.replace(R.id.fragment, fragment)
				.disallowAddToBackStack()
				.commit();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}

	private void showLoadingView() {
		resultsList.invalidate();
		mapFragment.invalidate();
		loaderIcon.startAnimation(loaderAnimation);
		loaderIcon.setVisibility(View.VISIBLE);
		errorText.setVisibility(View.GONE);
		fragmentContainer.setVisibility(View.GONE);
	}

	private void showErrorView(String error) {
		//TODO improve error view
		fragmentContainer.setVisibility(View.GONE);
		errorText.setVisibility(View.VISIBLE);
		errorText.setText(error);
		loaderIcon.clearAnimation();
		loaderIcon.setVisibility(View.GONE);
	}

	private void showResultsView() {
		loaderIcon.setVisibility(View.GONE);
		loaderIcon.clearAnimation();
		errorText.setVisibility(View.GONE);
		fragmentContainer.setVisibility(View.VISIBLE);
	}

	public void retrievePages(final Section section) {
		if (section.getPages() == null) {
			showLoadingView();
			pageService.getPagesInCategories(this.getApplicationContext(),
					//TODO fix this
					Arrays.asList(section.getCategory()),
					null, null,
					new PageService.Listener() {
						@Override
						public void onSuccess(List<Page> pages, final SearchResultsMeta meta) {
							section.setPages(pages);
							section.setSearchResultsMeta(meta);
							populateFragments(section);
						}

						@Override
						public void onFailure(Exception e) {
							showErrorView("ERROR: " + e.getMessage());
						}
					}, section);
		} else {
			populateFragments(section);
		}
	}

	private void populateFragments(Section section) {
		resultsList.showSection(section);
		mapFragment.showSection(section);
		showResultsView();
	}

	@Override
	public void onListItemClicked(AdapterView<?> parent, View view, int position, long id) {
		if (position < currentSection.getPages().size()) {
			Page page = currentSection.getPages().get(position);
			if (hasViewedFirstPage) {
			} else {
				hasViewedFirstPage = true;
			}
			showPageContent(page);
		}
	}

	@Override
	public void onLoadMoreButtonClicked(View v) {
		final Section section = currentSection;
		pageService.getNextSetOfResults(section.getSearchResultsMeta(),
				this.getApplicationContext(),
				new PageService.Listener() {
					@Override
					public void onSuccess(List<Page> pages, SearchResultsMeta meta) {
						section.getPages().addAll(pages);
						section.setSearchResultsMeta(meta);
						populateFragments(section);
					}

					@Override
					public void onFailure(Exception e) {
						showErrorView(e.getMessage());
					}
				}, section);
	}

	@Override
	public void onRefreshList() {
		currentSection.setPages(null);
		currentSection.setListPosition(0);
		showSection(currentSection);
	}

	@Override
	public void onMapPinInfoWindowClicked(Page associatedPage) {
		if (hasViewedFirstPage) {
		} else {
			hasViewedFirstPage = true;
		}
		showPageContent(associatedPage);
	}

	private void showPageContent(Page page) {
		PageContentActivity.setPage(page);
		Intent intent = new Intent(this, PageContentActivity.class);
		startActivity(intent);
	}
}
