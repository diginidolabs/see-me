/*
 * Copyright 2015 John Krause
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.activities;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.diginido.legacy.appseeme.R;
import com.diginido.seeme.Constants;
import com.diginido.seeme.fragments.RateServiceFragment;
import com.diginido.seeme.network.response.model.Page;
import com.diginido.seeme.network.response.model.Rating;
import com.diginido.seeme.network.response.model.ResourceImage;
import com.diginido.seeme.network.service.ImageService;
import com.diginido.seeme.network.service.PageService;
import com.diginido.seeme.network.service.RatingService;
import com.diginido.seeme.network.service.ServiceFactory;

import java.util.List;

public class PageContentActivity extends BaseActivity implements RateServiceFragment.OnRatingSelectedListener {

	private ImageService imageService = ServiceFactory.getImageService();
	private PageService pageService = ServiceFactory.getPageService();
	private ViewGroup imagesList;
	private View rateServiceButton;
	private RateServiceFragment rateServiceFragment;
	private RatingService ratingService = ServiceFactory.getRatingService();
	@DrawableRes
	private int defaultMainImage = R.drawable.icon_no_image_large;

	private static Page page;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (page != null) {
			setContentView(R.layout.activity_page_content);
			initViews();
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		pageService.cancel(this);
		imageService.cancel(this);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		initFragments();
	}

	private void initFragments() {
		rateServiceFragment = new RateServiceFragment();
		rateServiceFragment.setOnRatingSelectedListener(this);
	}

	private void initViews() {
		initImages();
		initText(R.id.content_title, page.getTitle());
		initText(R.id.content_address,
				page.getAddresses().size() > 0
						? page.getAddresses().get(0).getFullStreetAddress()
						: null);
		initText(R.id.content_hours, null);
		initText(R.id.content_phone,
				page.getPhoneNumbers().size() > 0
						? page.getPhoneNumbers().get(0).toString()
						: null);
		initText(R.id.content_website, page.getWebsite());
		initText(R.id.content_summary, page.getSummary());
		initNoTalkContent();
		rateServiceButton = findViewById(R.id.rate_service_button);
		rateServiceButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!rateServiceFragment.isAdded()) {
					rateServiceFragment.show(getSupportFragmentManager(), RateServiceFragment.class.getName());
				}
			}
		});
		initText(R.id.ratings_up, Integer.toString(page.getRatingCounts().getPositive()));
		initText(R.id.ratings_down, Integer.toString(page.getRatingCounts().getNegavite()));
	}

	private void initImages() {
		imagesList = (ViewGroup) findViewById(R.id.content_images_layout);
		List<ResourceImage> images = page.getResourceImages();
		if (images != null && images.size() > 0) {
			for (int i = 0; i < images.size(); i++) {
				ImageView imageView = (ImageView) getLayoutInflater().inflate(R.layout.page_content_image, imagesList, false);
				imageView.setImageResource(defaultMainImage);
				imagesList.addView(imageView);
				initImage(imageView, images.get(i));
			}
		} else {
			ImageView imageView = (ImageView) getLayoutInflater().inflate(R.layout.page_content_image, imagesList, false);
			imageView.setImageResource(defaultMainImage);
			imagesList.addView(imageView);
		}
	}

	private void initImage(final ImageView imageView, ResourceImage image) {
		ImageService.Listener listener = new ImageService.Listener() {
			@Override
			public void onSuccess(Bitmap image) {
				if (image != null) {
					imageView.setImageBitmap(image);
				}
			}

			@Override
			public void onFailure(Exception e) {
				Log.e(Constants.getLogTag(this), "Error getting image", e);
				if (imagesList.indexOfChild(imageView) != 0) {
					imageView.setVisibility(View.GONE);
				}
			}
		};
		imageService.getImage(image, ImageService.ImageSize.LARGE, this, listener, this);
	}

	private void initText(@IdRes int resourceId, String value) {
		if (value == null || value.length() <= 0) {
			findViewById(resourceId).setVisibility(View.GONE);
		} else {
			String text = value.trim();
			TextView textView = (TextView) findViewById(resourceId);
			textView.setText(text);
		}
	}

	private void initNoTalkContent() {
		initText(R.id.content_talk, getString(R.string.empty_talk_content));
	}

	@Override
	public void onRatingSelected(Rating.RatingOptions rating) {
		ratingService.updateRating(this, page, rating, new RatingService.Listener() {
			@Override
			public void onSuccess() {
				updateRatings();
			}

			@Override
			public void onFailure(Exception exception) {
				Log.e("RatingUpdate", "Error updating rating", exception);
				Toast.makeText(PageContentActivity.this, getString(R.string.rating_error_message), Toast.LENGTH_SHORT).show();
			}
		}, this);
	}

	private void updateRatings() {
		initText(R.id.ratings_up, Integer.toString(page.getRatingCounts().getPositive()));
		initText(R.id.ratings_down, Integer.toString(page.getRatingCounts().getNegavite()));
	}

	public static void setPage(Page page) {
		PageContentActivity.page = page;
	}
}
