/*
 * Copyright 2015 John Krause
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.activities;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.diginido.legacy.appseeme.R;

public class StartupActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_startup);
		init();
	}

	protected void init() {
		final View logo = findViewById(R.id.logo_image);
		final ObjectAnimator fadeIn = animateLogoIn(logo);
		final ObjectAnimator fadeOut = animateLogoOut(logo);
		fadeIn.addListener(new Animator.AnimatorListener() {
			@Override
			public void onAnimationStart(Animator animation) {
			}

			@Override
			public void onAnimationEnd(Animator animation) {
				fadeOut.start();
			}

			@Override
			public void onAnimationCancel(Animator animation) {
			}

			@Override
			public void onAnimationRepeat(Animator animation) {
			}
		});
		fadeOut.addListener(new Animator.AnimatorListener() {
			@Override
			public void onAnimationStart(Animator animation) {
			}

			@Override
			public void onAnimationEnd(Animator animation) {
				showHomeActivity();
			}

			@Override
			public void onAnimationCancel(Animator animation) {
			}

			@Override
			public void onAnimationRepeat(Animator animation) {
			}
		});
		fadeIn.start();
	}

	private ObjectAnimator animateLogoIn(View logo) {
		logo.setAlpha(0);
		ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(logo,
				"alpha", 0x0, 0xF).setDuration(2000);
		return objectAnimator;
	}

	private ObjectAnimator animateLogoOut(View logo) {
		ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(logo,
				"alpha", 0xF, 0x0).setDuration(2000);
		return objectAnimator;
	}

	private void showHomeActivity() {
		this.finish();
		startActivity(new Intent(getApplicationContext(), HomeActivity.class));
	}
}
