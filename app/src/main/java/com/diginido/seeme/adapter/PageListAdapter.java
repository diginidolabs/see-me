/*
 * Copyright 2015 John Krause
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.DrawableRes;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.diginido.legacy.appseeme.R;
import com.diginido.seeme.Constants;
import com.diginido.seeme.model.Section;
import com.diginido.seeme.network.response.model.Page;
import com.diginido.seeme.network.response.model.ResourceImage;
import com.diginido.seeme.network.service.ImageService;

import java.util.List;

/**
 * @author John Krause
 * @since 8/6/15
 */
public class PageListAdapter extends BaseAdapter {

	private List<Page> pageList;
	private LayoutInflater inflater;
	@DrawableRes
	private int backgroundDrawable, defaultItemDrawable;
	private ImageService imageService;
	private Context context;
	private Object tag;

	public PageListAdapter(Context context,
						   Section section,
						   ImageService imageService,
						   Object adapterUser) {
		this.context = context;
		this.inflater = LayoutInflater.from(context);
		this.pageList = section.getPages();
		defaultItemDrawable = R.drawable.icon_no_image_small;
		backgroundDrawable = section.getInfoContainer();
		this.imageService = imageService;
		this.tag = adapterUser;
	}

	@Override
	public int getCount() {
		return pageList.size();
	}

	@Override
	public Object getItem(int position) {
		return pageList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return pageList.get(position).getPageId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Page page = pageList.get(position);
		List<ResourceImage> images = page.getResourceImages();
		if (convertView == null) {
			convertView = this.inflater.inflate(R.layout.page_list_item, parent, false);
		}
		ImageView pageImage = (ImageView) convertView.findViewById(R.id.page_image);
		pageImage.setImageResource(defaultItemDrawable);
		if (images != null && images.size() > 0) {
			downloadItemImage(images.get(0), pageImage);
		}
		ViewGroup layout = (ViewGroup) convertView.findViewById(R.id.page_list_item_layout);
		layout.setBackgroundResource(backgroundDrawable);
		TextView pageName = (TextView) convertView.findViewById(R.id.page_name);
		pageName.setText(page.getTitle());
		TextView pageAddress = (TextView) convertView.findViewById(R.id.page_address);
		pageAddress.setText(page.getAddresses().size() > 0
				? page.getAddresses().get(0).getFullStreetAddress()
				: null);
		//TODO implement hours functionality
		return convertView;
	}

	private void downloadItemImage(final ResourceImage images, final ImageView pageImage) {
		final PageListAdapter pageListAdapter = this;
		imageService.getImage(images, ImageService.ImageSize.MEDIUM,
				context, new ImageService.Listener() {
					@Override
					public void onSuccess(Bitmap image) {
						pageImage.setImageBitmap(image);
						pageListAdapter.notifyDataSetChanged();
					}

					@Override
					public void onFailure(Exception e) {
						Log.e(Constants.getLogTag(this), "Error getting list item image", e);
					}
				}, tag);
	}
}
