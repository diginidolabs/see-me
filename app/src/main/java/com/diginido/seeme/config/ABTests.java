/*
 * Copyright 2015 John Krause & Shelter Tech
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.config;

import android.content.Context;
import android.content.SharedPreferences;

import com.diginido.seeme.utils.Analytics;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * @author John Krause
 * @since 9/7/15
 */
public class ABTests {

	private static final String SHARED_PREFS = "abTestsSeeMe";
	public static final String HOME_SCREEN_ICONS_TEST = "homeScreenIconsABTest";
	private static SharedPreferences sharedPreferences;

	public static void init(Context context) {
		sharedPreferences = context.getApplicationContext().getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
		initTests();
	}

	private static void initTests() {
		isTestOn(HOME_SCREEN_ICONS_TEST);
	}

	public static boolean isTestOn(String test) {
		if (!sharedPreferences.contains(test)) {
			boolean testValue = new Random().nextBoolean();
			sharedPreferences.edit().putBoolean(test, testValue).commit();

			// record ab test setup event in analytics
			Map<String, String> segmentation = new HashMap<>(1);
			segmentation.put("value", Boolean.toString(testValue));
			Analytics.recordABTestInitEvent(test, segmentation);
			return testValue;
		} else {
			return sharedPreferences.getBoolean(test, false);
		}
	}
}
