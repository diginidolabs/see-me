/*
 * Copyright 2015 John Krause
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.diginido.legacy.appseeme.R;
import com.diginido.seeme.map.MainMapView;
import com.diginido.seeme.map.PageMarker;
import com.diginido.seeme.model.Section;
import com.diginido.seeme.network.response.model.Address;
import com.diginido.seeme.network.response.model.Page;
import com.diginido.seeme.utils.LocationHelper;

import org.osmdroid.tileprovider.constants.OpenStreetMapTileProviderConstants;
import org.osmdroid.util.GeoPoint;

/**
 * @author John Krause
 * @since 8/16/15
 */
public class MapFragment extends Fragment implements OpenStreetMapTileProviderConstants {

	private MainMapView mapView;
	private Section currentSection;
	private LocationHelper locationHelper;

	public interface OnMapActionListener {
		void onMapPinInfoWindowClicked(Page associatedPage);
	}

	private OnMapActionListener onMapActionListener;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View root = inflater.inflate(R.layout.fragment_map, container, false);
		mapView = (MainMapView) root.findViewById(R.id.map);
		mapView.initMap(getActivity(), root);
		locationHelper = new LocationHelper(getActivity());
		return root;
	}

	@Override
	public void onResume() {
		super.onResume();
		getPinsFromPages();
		if (!mapView.enableMyLocation()) {
			locationHelper.promptIfLocationNotAvailable();
			mapView.gotoMyLocation();
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		mapView.disableMyLocation();
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mapView.onDetach();
	}

	private void getPinsFromPages() {
		if (currentSection != null && currentSection.getPages() != null && !currentSection.getPages().isEmpty()) {
			for (final Page page : currentSection.getPages()) {
				if (page.getAddresses().size() > 0) {
					Address address = page.getAddresses().get(0);
					if (address.getLatitude() != 0.0 && address.getLongitude() != 0.0) {
						PageMarker marker = new PageMarker(mapView, page);
						marker.setIcon(getResources().getDrawable(currentSection.getMapPin()));
						marker.setPosition(new GeoPoint(address.getLatitude(), address.getLongitude()));
						marker.setTitle(page.getTitle());
						marker.setSnippet(page.getSummary());
						marker.setOnInfoWindowShownListener(new PageMarker.OnInfoWindowShownListener() {
							@Override
							public void onInfoWindowShown(PageMarker marker) {
								mapView.setLastOpenedMarker(marker);
							}
						});
						marker.setOnInfoWindowClickedListener(new PageMarker.OnInfoWindowClickedListener() {
							@Override
							public void onInfoWindowClicked(PageMarker marker) {
								onMapActionListener.onMapPinInfoWindowClicked(marker.getPage());
							}
						});
						mapView.getOverlays().add(marker);
						mapView.invalidate();
					}
				}
			}
		}
	}

	public void showSection(Section section) {
		currentSection = section;
		invalidate();
		if (isVisible()) {
			getPinsFromPages();
			mapView.gotoMyLocation();
		}
	}

	public void invalidate() {
		if (mapView != null) {
			mapView.reset();
			mapView.gotoMyLocation();
		}
	}

	public void setOnMapActionListener(OnMapActionListener onMapActionListener) {
		this.onMapActionListener = onMapActionListener;
	}

}
