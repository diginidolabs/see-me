/*
 * Copyright 2015 John Krause & Shelter Tech
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.fragments;

import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.internal.view.menu.MenuBuilder;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.diginido.legacy.appseeme.R;
import com.diginido.seeme.model.Filters;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author John Krause
 * @since 9/13/15
 */
public class RadialMenuFragment extends Fragment {

	private static final double TWO_PI = Math.PI * 2, PI_OVER_2 = Math.PI / 2;
	private static final long DURATION_DEPLOY = 100, DURATION_EXPAND = 100,
			DURATION_BG_FADE = DURATION_DEPLOY + DURATION_EXPAND;
	private static final Object BACKGROUND_START = 0x00333333, BACKGROUND_END = 0xBB333333;

	public interface OnFinalSelectionListener {
		void onFinalSelectionMade(int sectionMenuId, Filters filters);
	}

	private OnFinalSelectionListener onFinalSelectionListener;
	private Menu menu;
	private ViewGroup layout;
	private Context context;
	private float originX = 0, originY = 0;
	private Map<ImageView, MenuItem> currentlyDisplayedItems;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_radial_menu, container, false);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		layout = (ViewGroup) view;
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		initViews();
	}

	private void initViews() {
		layout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
		context = layout.getContext();
		menu = new MenuBuilder(context);
		new MenuInflater(context).inflate(R.menu.radial_menu, menu);
		currentlyDisplayedItems = new LinkedHashMap<>(menu.size());
		for (int i = 0; i < menu.size(); i++) {
			ImageView button = new ImageView(context);
			button.setImageDrawable(menu.getItem(i).getIcon());
			button.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
					ViewGroup.LayoutParams.WRAP_CONTENT));
			button.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					showSubSections(v);
				}
			});
			layout.addView(button);
			currentlyDisplayedItems.put(button, menu.getItem(i));
		}
		initStartAnimation();
	}

	private void initStartAnimation() {
		Point size = new Point();
		getActivity().getWindowManager().getDefaultDisplay().getSize(size);
		Iterator<ImageView> iterator = currentlyDisplayedItems.keySet().iterator();
		for (int i = 0; i < currentlyDisplayedItems.size(); i++) {
			final ImageView v = iterator.next();
			v.setTranslationX(originX);
			v.setTranslationY(originY);
			float xOffset = v.getDrawable().getIntrinsicWidth()
					* 2 * (float) Math.cos((TWO_PI / currentlyDisplayedItems.size()) * i - PI_OVER_2);
			float yOffset = v.getDrawable().getIntrinsicHeight()
					* 2 * (float) Math.sin((TWO_PI / currentlyDisplayedItems.size()) * i - PI_OVER_2);
			float centerY = (size.y / 2) - getStatusBarHeight() - (v.getDrawable().getIntrinsicHeight() / 2);
			float centerX = (size.x / 2) - (v.getDrawable().getIntrinsicWidth() / 2);
			AnimatorSet moveAnim = new AnimatorSet();
			ObjectAnimator xAnim = ObjectAnimator.ofFloat(v, "x", centerX);
			ObjectAnimator yAnim = ObjectAnimator.ofFloat(v, "y", centerY);
			moveAnim.play(yAnim).with(xAnim);
			moveAnim.setDuration(DURATION_DEPLOY);

			AnimatorSet spreadAnim = new AnimatorSet();
			ObjectAnimator spreadAnimX = ObjectAnimator.ofFloat(v, "x", centerX + xOffset);
			ObjectAnimator spreadAnimY = ObjectAnimator.ofFloat(v, "y", centerY + yOffset);
			spreadAnim.play(spreadAnimX).with(spreadAnimY);
			spreadAnim.setDuration(DURATION_EXPAND);

			AnimatorSet animatorSet = new AnimatorSet();
			animatorSet.play(moveAnim).before(spreadAnim);
			animatorSet.start();
		}

		ObjectAnimator backgroundFade = ObjectAnimator
				.ofObject(layout, "backgroundColor", new ArgbEvaluator(),
						BACKGROUND_START, BACKGROUND_END);
		backgroundFade.setDuration(DURATION_BG_FADE);
		backgroundFade.start();

	}

	public int getStatusBarHeight() {
		int result = 0;
		int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
		if (resourceId > 0) {
			result = getResources().getDimensionPixelSize(resourceId);
		}
		return result;
	}

	private void showSubSections(View v) {
		dismiss();
		onFinalSelectionListener.onFinalSelectionMade(currentlyDisplayedItems.get(v).getItemId(), null);
	}

	private void dismiss() {
		if (!getActivity().getSupportFragmentManager().popBackStackImmediate()) {
			getActivity().getSupportFragmentManager().beginTransaction()
					.disallowAddToBackStack()
					.remove(this)
					.commit();
		}
	}

	public void show(View originView, AppCompatActivity activity) {
		int[] origin = new int[2];
		originView.getLocationInWindow(origin);
		DisplayMetrics dm = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
		int height = activity.getWindow().getDecorView().getRootView().getMeasuredHeight();
		int topOffset = dm.heightPixels - height;
		show(origin[0], origin[1] - topOffset, activity.getSupportFragmentManager());
	}

	public void show(float startX, float startY, FragmentManager fragmentManager) {
		originX = startX;
		originY = startY;
		fragmentManager.beginTransaction()
				.add(R.id.root, this, this.getClass().getName())
				.addToBackStack(this.getClass().getName())
				.commit();
	}

	public void setOnFinalSelectionListener(OnFinalSelectionListener onFinalSelectionListener) {
		this.onFinalSelectionListener = onFinalSelectionListener;
	}
}
