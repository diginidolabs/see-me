/*
 * Copyright 2015 John Krause
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.diginido.legacy.appseeme.R;
import com.diginido.seeme.network.response.model.Rating;


/**
 * @author John Krause
 * @since 9/3/15
 */
public class RateServiceFragment extends DialogFragment {

	public interface OnRatingSelectedListener {
		void onRatingSelected(Rating.RatingOptions rating);
	}

	private View goodServiceButton, badServiceButton, noServiceButton;
	private OnRatingSelectedListener onRatingSelectedListener;

	public RateServiceFragment() {
		this.setStyle(STYLE_NO_FRAME, 0);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_rate_service, container);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		initViews(view);
		this.getDialog().setCanceledOnTouchOutside(true);
	}

	private void initViews(View root) {
		//TODO implement rating service to hold rating values
		goodServiceButton = root.findViewById(R.id.good_service_button);
		goodServiceButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				goodService();
			}
		});
		badServiceButton = root.findViewById(R.id.bad_service_button);
		badServiceButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				badService();
			}
		});
		noServiceButton = root.findViewById(R.id.no_service_button);
		noServiceButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				noService();
			}
		});
	}

	private void goodService() {
		this.dismiss();
		onRatingSelectedListener.onRatingSelected(Rating.RatingOptions.POSITIVE);
	}

	private void badService() {
		this.dismiss();
		onRatingSelectedListener.onRatingSelected(Rating.RatingOptions.NEGATIVE);
	}

	private void noService() {
		this.dismiss();
		onRatingSelectedListener.onRatingSelected(Rating.RatingOptions.NO_SERVICE);
	}

	public void setOnRatingSelectedListener(OnRatingSelectedListener onRatingSelectedListener) {
		this.onRatingSelectedListener = onRatingSelectedListener;
	}
}
