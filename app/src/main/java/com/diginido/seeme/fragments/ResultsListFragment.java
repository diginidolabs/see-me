/*
 * Copyright 2015 John Krause
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.diginido.legacy.appseeme.R;
import com.diginido.seeme.adapter.PageListAdapter;
import com.diginido.seeme.model.Section;
import com.diginido.seeme.network.service.ImageService;
import com.diginido.seeme.network.service.ServiceFactory;


/**
 * The fragment that contains the results list.
 */
public class ResultsListFragment extends Fragment {

	private Section currentSection;
	private ListView listView;
	private View loadMoreButton;
	private SwipeRefreshLayout swipeRefreshLayout;
	private final ImageService imageService = ServiceFactory.getImageService();

	public interface OnResultsListActionListener {
		void onListItemClicked(AdapterView<?> parent, View view, int position, long id);

		void onLoadMoreButtonClicked(View v);

		void onRefreshList();
	}

	private OnResultsListActionListener actionListener;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		swipeRefreshLayout =
				(SwipeRefreshLayout) inflater.inflate(R.layout.fragment_results_list, container, false);
		swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				swipeRefreshLayout.setRefreshing(false);
				actionListener.onRefreshList();
			}
		});
		listView = (ListView) swipeRefreshLayout.findViewById(R.id.results_list);
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				actionListener.onListItemClicked(parent, view, position, id);
			}
		});
		loadMoreButton = inflater.inflate(R.layout.page_list_load_more, listView, false);
		loadMoreButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				loadMoreButton.setClickable(false);
				loadMoreButton.setAlpha(0.2f);
				currentSection.setListPosition(listView.getFirstVisiblePosition() + 1);
				actionListener.onLoadMoreButtonClicked(v);
			}
		});
		return swipeRefreshLayout;
	}

	@Override
	public void onResume() {
		super.onResume();
		populateList();
	}

	public void showSection(Section section) {
		if (currentSection != null && listView != null && currentSection != section) {
			currentSection.setListPosition(listView.getFirstVisiblePosition());
		}
		currentSection = section;
		if (isVisible()) {
			populateList();
		}
	}

	public void invalidate() {
		currentSection = null;
		if (listView != null) {
			listView.setAdapter(null);
			listView.removeFooterView(loadMoreButton);
		}
	}

	private void populateList() {
		if (currentSection != null && listView != null) {
			listView.setAdapter(new PageListAdapter(getActivity(), currentSection,
					imageService, this));
			if (currentSection.getSearchResultsMeta().getNextPage() != 0) {
				if (listView.getFooterViewsCount() == 0) {
					listView.addFooterView(loadMoreButton);
				}
				loadMoreButton.setClickable(true);
				loadMoreButton.setAlpha(1);
			} else {
				listView.removeFooterView(loadMoreButton);
			}
			listView.setSelectionFromTop(currentSection.getListPosition(), 0);
		} else {
			invalidate();
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		imageService.cancel(this);
		if (currentSection != null && listView != null) {
			currentSection.setListPosition(listView.getFirstVisiblePosition());
		}
	}

	public void setActionListener(OnResultsListActionListener actionListener) {
		this.actionListener = actionListener;
	}
}
