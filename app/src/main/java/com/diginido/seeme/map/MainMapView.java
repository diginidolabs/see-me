/*
 * Copyright 2015 John Krause
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.map;

import android.content.Context;
import android.graphics.Rect;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.diginido.legacy.appseeme.R;
import com.diginido.seeme.Constants;
import com.diginido.seeme.utils.NetworkUtils;

import org.osmdroid.ResourceProxy;
import org.osmdroid.tileprovider.MapTileProviderBase;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;

import java.util.Collections;

/**
 * @author John Krause
 * @since 9/27/15
 */
//TODO: figure out why this class takes up so much memory and CPU with no network connection
public class MainMapView extends MapView {

	private PageMarker lastOpenedMarker;
	private float x1, x2;
	private static final float MIN_DISTANCE = 10;
	private MyLocationOverlay myLocationOverlay;
	private boolean waitingForLocationFix;
	private View locateMeButton;
	private static final int DEFAULT_ZOOM = 13;

	protected MainMapView(Context context, int tileSizePixels, ResourceProxy resourceProxy, MapTileProviderBase tileProvider, Handler tileRequestCompleteHandler, AttributeSet attrs) {
		super(context, tileSizePixels, resourceProxy, tileProvider, tileRequestCompleteHandler, attrs);
	}

	public MainMapView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public MainMapView(Context context, int tileSizePixels) {
		super(context, tileSizePixels);
	}

	public MainMapView(Context context, int tileSizePixels, ResourceProxy resourceProxy) {
		super(context, tileSizePixels, resourceProxy);
	}

	public MainMapView(Context context, int tileSizePixels, ResourceProxy resourceProxy, MapTileProviderBase aTileProvider) {
		super(context, tileSizePixels, resourceProxy, aTileProvider);
	}

	public MainMapView(Context context, int tileSizePixels, ResourceProxy resourceProxy, MapTileProviderBase aTileProvider, Handler tileRequestCompleteHandler) {
		super(context, tileSizePixels, resourceProxy, aTileProvider, tileRequestCompleteHandler);
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {
		boolean handled = super.dispatchTouchEvent(event);
		if (lastOpenedMarker != null) {
			switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					x1 = event.getX();
					break;
				case MotionEvent.ACTION_UP:
					x2 = event.getX();
					float deltaX = x2 - x1;
					if (Math.abs(deltaX) < MIN_DISTANCE &&
							isWithinView(event, lastOpenedMarker.getInfoWindow().getView())) {
						lastOpenedMarker.onInfoWindowClicked();
						return true;
					}
					break;
			}
		}
		return handled;
	}

	private boolean isWithinView(MotionEvent event, View view) {
		int[] location = new int[2];
		view.getLocationInWindow(location);
		Rect region = new Rect(location[0], location[1],
				location[0] + view.getWidth(), location[1] + view.getHeight());
		return region.contains((int) event.getRawX(), (int) event.getRawY());
	}

	public void setLastOpenedMarker(PageMarker lastOpenedMarker) {
		this.lastOpenedMarker = lastOpenedMarker;
	}

	public void reset() {
		clearFocus();
		invalidate();
		if (lastOpenedMarker != null) {
			lastOpenedMarker.closeInfoWindow();
			lastOpenedMarker = null;
		}
		getOverlays().retainAll(Collections.singletonList(myLocationOverlay));
	}

	public void initMap(Context context, View rootView) {
		if (NetworkUtils.isNetworkAvailable(context)) {
			setUseDataConnection(true);
		} else {
			setUseDataConnection(false);
		}
		locateMeButton = rootView.findViewById(R.id.locate_me_button);
		setBuiltInZoomControls(true);
		setMultiTouchControls(true);
		setTileSource(TileSourceFactory.MAPNIK);

		myLocationOverlay = new MyLocationOverlay(context, this);
		myLocationOverlay.setOnLocationChangedListener(new MyLocationOverlay.OnLocationChangedListener() {
			@Override
			public void onLocationChanged(android.location.Location location) {
				if (waitingForLocationFix) {
					gotoMyLocation(location);
				}
			}
		});

		getOverlays().add(myLocationOverlay);
		getController().setZoom(DEFAULT_ZOOM);
		getController().setCenter(Constants.DEFAUL_LOCATION);
		setTilesScaledToDpi(true);

		locateMeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				gotoMyLocation();
			}
		});

		gotoMyLocation();
	}

	public void gotoMyLocation() {
		gotoMyLocation(null);
	}

	public void gotoMyLocation(android.location.Location location) {
		if (location != null) {
			getController().animateTo(new GeoPoint(location.getLatitude(), location.getLongitude()));
			waitingForLocationFix = false;
			locateMeButton.setAlpha(1.0f);
		} else if (myLocationOverlay.getMyLocation() != null) {
			getController().animateTo(myLocationOverlay.getMyLocation());
			waitingForLocationFix = false;
			locateMeButton.setAlpha(1.0f);
		} else {
			waitingForLocationFix = true;
			locateMeButton.setAlpha(0.5f);
		}
	}

	public boolean enableMyLocation() {
		return myLocationOverlay.enableMyLocation();
	}

	public void disableMyLocation() {
		myLocationOverlay.disableMyLocation();
	}
}
