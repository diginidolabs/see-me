/*
 * Copyright 2015 John Krause
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.map;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.location.Location;

import com.diginido.legacy.appseeme.R;

import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.IMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;


/**
 * @author John Krause
 * @since 8/30/15
 */
public class MyLocationOverlay extends MyLocationNewOverlay {

	public interface OnLocationChangedListener {
		void onLocationChanged(Location location);
	}

	private OnLocationChangedListener onLocationChangedListener;

	public MyLocationOverlay(final Context context, MapView mapView) {
		super(new GpsMyLocationProvider(context), mapView, new DefaultResourceProxyImpl(context) {
			// This whole section is simply to override the bitmap returned for the person image
			// This should be removed when a better way to do this is implemented
			@Override
			public Bitmap getBitmap(bitmap pResId) {
				if (pResId.equals(bitmap.person) || pResId.equals(bitmap.direction_arrow)) {
					return BitmapFactory.decodeResource(context.getResources(), R.drawable.icon_map_pin_you);
				}
				return super.getBitmap(pResId);
			}
		});
		Drawable you = context.getResources().getDrawable(R.drawable.icon_map_pin_you);
		mPersonHotspot.set(you.getIntrinsicWidth() / 2, you.getIntrinsicHeight());
	}

	@Override
	public void onLocationChanged(Location location, IMyLocationProvider source) {
		super.onLocationChanged(location, source);
		if (onLocationChangedListener != null) {
			onLocationChangedListener.onLocationChanged(location);
		}
	}

	public void setOnLocationChangedListener(OnLocationChangedListener listener) {
		this.onLocationChangedListener = listener;
	}
}
