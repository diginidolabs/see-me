/*
 * Copyright 2015 John Krause
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.map;

import com.diginido.legacy.appseeme.R;
import com.diginido.seeme.SeeMeApplication;
import com.diginido.seeme.network.response.model.Page;

import org.osmdroid.ResourceProxy;
import org.osmdroid.bonuspack.overlays.Marker;
import org.osmdroid.views.MapView;


/**
 * @author John Krause
 * @since 9/27/15
 */
public class PageMarker extends Marker {

	private Page page;
	private static final int MAX_SNIPPET_CHARS = 150;
	private String snippetFormat;

	public interface OnInfoWindowShownListener {
		void onInfoWindowShown(PageMarker marker);
	}

	public interface OnInfoWindowClickedListener {
		void onInfoWindowClicked(PageMarker marker);
	}

	private OnInfoWindowShownListener onInfoWindowShownListener;
	private OnInfoWindowClickedListener onInfoWindowClickedListener;

	public PageMarker(MapView mapView, Page page) {
		super(mapView);
		this.page = page;
		init();
	}

	public PageMarker(MapView mapView, Page page, ResourceProxy resourceProxy) {
		super(mapView, resourceProxy);
		this.page = page;
		init();
	}

	private void init() {
		mInfoWindow.getView().setOnTouchListener(null);
		snippetFormat = SeeMeApplication.getAppContext()
				.getString(R.string.info_window_snippet_format);
	}

	@Override
	public void showInfoWindow() {
		super.showInfoWindow();
		onInfoWindowShownListener.onInfoWindowShown(this);
	}

	public void setOnInfoWindowShownListener(OnInfoWindowShownListener onInfoWindowShownListener) {
		this.onInfoWindowShownListener = onInfoWindowShownListener;
	}

	public void setOnInfoWindowClickedListener(OnInfoWindowClickedListener onInfoWindowClickedListener) {
		this.onInfoWindowClickedListener = onInfoWindowClickedListener;
	}

	public void onInfoWindowClicked() {
		onInfoWindowClickedListener.onInfoWindowClicked(this);
	}

	public Page getPage() {
		return page;
	}

	@Override
	protected boolean onMarkerClickDefault(Marker marker, MapView mapView) {
		if (marker == this) {
			if (this.isInfoWindowShown()) {
				this.closeInfoWindow();
			} else {
				this.showInfoWindow();
				if (this.mPanToView) {
					mapView.getController().animateTo(marker.getPosition());
				}
			}
			return true;
		} else {
			return super.onMarkerClickDefault(marker, mapView);
		}
	}

	@Override
	public void setSnippet(String snippet) {
		if (snippet != null) {
			String truncated = snippet.length() > MAX_SNIPPET_CHARS
					? String.format(snippetFormat, snippet.substring(0, MAX_SNIPPET_CHARS))
					: String.format(snippetFormat, snippet);
			super.setSnippet(truncated);
		} else {
			super.setSnippet(snippet);
		}
	}
}
