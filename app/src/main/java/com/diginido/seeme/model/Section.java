/*
 * Copyright 2015 John Krause
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.model;

import android.support.annotation.DrawableRes;

import com.diginido.legacy.appseeme.R;
import com.diginido.seeme.network.response.model.Category;
import com.diginido.seeme.network.response.model.Page;
import com.diginido.seeme.network.response.model.SearchResultsMeta;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author John Krause
 * @since 8/19/15
 */
public class Section {

	private static class SectionCategory implements Category {
		int id;
		String name;

		public SectionCategory(int id, String name) {
			this.id = id;
			this.name = name;
		}

		@Override
		public int getId() {
			return id;
		}

		@Override
		public String getName() {
			return name;
		}
	}

	public enum Sections {
		SHELTER,
		EATS,
		FACILITIES,
		EMPLOYMENT,
		HEALTH,
		LEGAL
	}

	public enum Icons {
		SMALL_ICON,
		SMALL_ICON_COLORED,
		LARGE_ICON
	}

	private static Section SHELTER() {
		return new Section("shelter", new SectionCategory(112, null),
				R.drawable.background_rest, R.drawable.text_header_shelter,
				R.drawable.icon_small_rest_orange, R.drawable.icon_small_rest_blue,
				R.drawable.icon_big_rest_orange,
				R.drawable.container_rest, R.drawable.icon_map_pin_rest);
	}

	private static Section EATS() {
		return new Section("eats", new SectionCategory(57, null),
				R.drawable.background_eats, R.drawable.text_header_eats,
				R.drawable.icon_small_eats_orange, R.drawable.icon_small_eats_red,
				R.drawable.icon_big_eats_orange,
				R.drawable.container_eats, R.drawable.icon_map_pin_eat);
	}

	private static Section FACILITIES() {
		return new Section("facilities", new SectionCategory(12, null),
				R.drawable.background_facilities, R.drawable.text_header_facilities,
				R.drawable.icon_small_facilities_orange, R.drawable.icon_small_facilities_blue,
				R.drawable.icon_big_facilities_orange,
				R.drawable.container_facilities, R.drawable.icon_map_pin_facilities);
	}

	private static Section EMPLOYMENT() {
		return new Section("employment", new SectionCategory(13, null), R.drawable.background_work,
				R.drawable.text_header_employment,
				R.drawable.icon_small_work_orange, R.drawable.icon_small_work_brown,
				R.drawable.icon_big_work_orange,
				R.drawable.container_work, R.drawable.icon_map_pin_work);
	}

	private static Section HEALTH() {
		return new Section("health", new SectionCategory(25, null),
				R.drawable.background_health, R.drawable.text_header_health,
				R.drawable.icon_small_medical_orange, R.drawable.icon_small_medical_pink,
				R.drawable.icon_big_medical_orange,
				R.drawable.container_health, R.drawable.icon_map_pin_health);
	}

	private static Section LEGAL() {
		return new Section("legal", new SectionCategory(15, null),
				R.drawable.background_legal, R.drawable.text_header_legal,
				R.drawable.icon_small_legal_orange, R.drawable.icon_small_legal_black,
				R.drawable.icon_big_legal_orange,
				R.drawable.container_legal, R.drawable.icon_map_pin_legal);
	}

	private static Map<Sections, Section> sectionsMap =
			new HashMap<Sections, Section>(Sections.values().length) {{
				put(Sections.EATS, EATS());
				put(Sections.FACILITIES, FACILITIES());
				put(Sections.HEALTH, HEALTH());
				put(Sections.LEGAL, LEGAL());
				put(Sections.SHELTER, SHELTER());
				put(Sections.EMPLOYMENT, EMPLOYMENT());
			}};

	public static Section getSection(Sections section) {
		return sectionsMap.get(section);
	}

	private List<Page> pages;
	private SearchResultsMeta searchResultsMeta;
	private int listPosition;
	private String name;
	private Category category;

	@DrawableRes
	private int background, headerText, infoContainer, mapPin;
	@DrawableRes
	private Map<Icons, Integer> icons = new HashMap<>(Icons.values().length);

	Section(String name, Category category,
			@DrawableRes int background,
			@DrawableRes int headerText,
			@DrawableRes int smallIcon,
			@DrawableRes int smallIconColored,
			@DrawableRes int largeIcon,
			@DrawableRes int infoContainer,
			@DrawableRes int mapPin) {
		this.category = category;
		this.background = background;
		this.headerText = headerText;
		this.infoContainer = infoContainer;
		listPosition = 0;
		this.mapPin = mapPin;
		this.name = name;
		icons.put(Icons.SMALL_ICON, smallIcon);
		icons.put(Icons.SMALL_ICON_COLORED, smallIconColored);
		icons.put(Icons.LARGE_ICON, largeIcon);
	}

	public Category getCategory() {
		return category;
	}

	public int getBackground() {
		return background;
	}

	public int getHeaderText() {
		return headerText;
	}

	public int getInfoContainer() {
		return infoContainer;
	}

	public List<Page> getPages() {
		return pages;
	}

	public void setPages(List<Page> pages) {
		this.pages = pages;
	}

	public SearchResultsMeta getSearchResultsMeta() {
		return searchResultsMeta;
	}

	public void setSearchResultsMeta(SearchResultsMeta searchResultsMeta) {
		this.searchResultsMeta = searchResultsMeta;
	}

	public int getListPosition() {
		return listPosition;
	}

	public void setListPosition(int listPosition) {
		this.listPosition = listPosition;
	}

	public int getMapPin() {
		return mapPin;
	}

	public String getName() {
		return name;
	}

	public int getIconDrawableResId(Icons icon) {
		return icons.get(icon);
	}
}
