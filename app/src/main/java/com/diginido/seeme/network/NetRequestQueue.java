/*
 * Copyright 2015 John Krause
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.network;

import android.content.Context;

import com.android.volley.Cache;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.diginido.legacy.appseeme.R;
import com.diginido.seeme.network.cache.DiskBasedImageCache;
import com.diginido.seeme.utils.NetworkUtils;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import ly.count.android.sdk.OpenUDIDAdapter;

/**
 * @author John Krause on 8/2/15.
 */
public class NetRequestQueue {
	private static NetRequestQueue sInstance;
	private RequestQueue mRequestQueue;
	private static Context sContext;
	private ImageLoader mImageLoader;
	private static final int CACHE_SIZE = 50 * 1024 * 1024;

	private NetRequestQueue(Context context) {
		sContext = context;
		mRequestQueue = getRequestQueue();
	}

	public RequestQueue getRequestQueue() {
		if (mRequestQueue == null) {
			mRequestQueue = Volley.newRequestQueue(sContext.getApplicationContext(), null, CACHE_SIZE);
			mRequestQueue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<Object>() {
				@Override
				public void onRequestFinished(Request<Object> request) {
					if (request.getCacheEntry() != null) {
						mRequestQueue.getCache().put(request.getCacheKey(), request.getCacheEntry());
					}
				}
			});
		}
		return mRequestQueue;
	}

	public ImageLoader getImageLoader() {
		if (mImageLoader == null) {
			mImageLoader = new ImageLoader(getRequestQueue(), new DiskBasedImageCache(sContext));
		}
		return mImageLoader;
	}

	public void addToRequestQueue(Request req, Response.Listener successListener, boolean useCache) {
		if (useCache && !retrieveFromCacheIfOffline(req, successListener)) {
			getRequestQueue().add(req);
		} else if (!useCache) {
			getRequestQueue().add(req);
		}
	}

	private boolean retrieveFromCacheIfOffline(Request request, Response.Listener successListener) {
		boolean retrieved = false;
		Cache.Entry entry = getRequestQueue().getCache().get(request.getCacheKey());
		if (entry != null && !NetworkUtils.isNetworkAvailable(sContext)) {
			if (successListener != null) {
				dispatchCachedResponse(successListener, entry);
				retrieved = true;
			}
		}
		return retrieved;
	}

	private void dispatchCachedResponse(Response.Listener<String> listener, Cache.Entry entry) {
		try {
			listener.onResponse(new String(entry.data,
					HttpHeaderParser.parseCharset(entry.responseHeaders)));
		} catch (UnsupportedEncodingException e) {
			listener.onResponse(new String(entry.data));
		}
	}

	public static NetRequestQueue getInstance(Context context) {
		if (sInstance == null) {
			sInstance = new NetRequestQueue(context);
		}
		return sInstance;
	}

	public Map<String, String> getDefaultRequestHeaders() {
		Map<String, String> headers = new HashMap<>();
		headers.put("User-Agent", sContext.getString(R.string.default_user_agent));
		headers.put("DEVICE-ID", OpenUDIDAdapter.getOpenUDID());
		return headers;
	}
}
