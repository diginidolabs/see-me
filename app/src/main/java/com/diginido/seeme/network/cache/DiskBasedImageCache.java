/*
 * Copyright 2015 John Krause
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.network.cache;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v4.util.LruCache;
import android.util.Log;

import com.android.volley.toolbox.ImageLoader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.regex.Pattern;

/**
 * @author John Krause
 * @since 10/9/15
 */
public class DiskBasedImageCache extends LruCache<String, Bitmap> implements ImageLoader.ImageCache {

	private Context mContext;
	private Pattern urlCleanPattern;
	private static final int LRU_CACHE_SIZE = 150;

	public DiskBasedImageCache(Context context) {
		super(LRU_CACHE_SIZE);
		mContext = context;
		urlCleanPattern = Pattern.compile("[^a-zA-Z0-9]");
	}

	@Override
	public Bitmap getBitmap(String url) {
		Bitmap bitmap = get(url);
		if (bitmap == null) {
			bitmap = getImageFromStorage(cleanUrl(url));
			if (bitmap != null) {
				put(url, bitmap);
			}
		}
		return bitmap;
	}

	@Override
	public void putBitmap(String url, Bitmap bitmap) {
		put(url, bitmap);
		new SaveImageTask().execute(cleanUrl(url), bitmap, mContext);
	}

	private Bitmap getImageFromStorage(String filename) {
		Bitmap bitmap = null;
		File filePath = mContext.getFileStreamPath(filename);
		if (filePath.exists()) {
			try {
				FileInputStream fi = new FileInputStream(filePath);
				bitmap = BitmapFactory.decodeStream(fi);
				fi.close();
			} catch (IOException e) {
				//no op
			}
		}
		return bitmap;
	}

	private String cleanUrl(String url) {
		return urlCleanPattern.matcher(url).replaceAll("");
	}

	private class SaveImageTask extends AsyncTask<Object, Void, Void> {
		@Override
		protected Void doInBackground(Object... params) {
			String filename = (String) params[0];
			Bitmap bitmap = (Bitmap) params[1];
			Context context = (Context) params[2];
			saveImageToInternalStorage(filename, bitmap, context);
			return null;
		}

		private void saveImageToInternalStorage(String filename, Bitmap image, Context context) {
			try {
				FileOutputStream fos = context.openFileOutput(filename, Context.MODE_PRIVATE);
				image.compress(Bitmap.CompressFormat.PNG, 100, fos);
				fos.close();
			} catch (IOException e) {
				Log.e(DiskBasedImageCache.class.getName(), "Error caching image to disk", e);
			}
		}
	}

	;

}
