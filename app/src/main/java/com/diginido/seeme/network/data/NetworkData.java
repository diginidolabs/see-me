/*
 * Copyright 2015 John Krause
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.network.data;

import android.content.Context;

import com.diginido.legacy.appseeme.R;
import com.diginido.seeme.SeeMeApplication;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * @author John Krause on 8/2/15.
 */
public class NetworkData {
	private static NetworkData sInstance;
	private Context mContext;
	private SimpleDateFormat mDateFormat;
	private Gson gson;

	private NetworkData() {
	}

	public static NetworkData getInstance() {
		if (sInstance == null) {
			synchronized (NetworkData.class) {
				sInstance = new NetworkData();
			}
		}
		sInstance.mContext = SeeMeApplication.getAppContext();
		return sInstance;
	}

	public String getHost() {
		//TODO switch depending on provider
		//return mContext.getString(R.string.host);
		return mContext.getString(R.string.herokuhost);
	}

	public SimpleDateFormat getDateFormat() {
		if (mDateFormat == null) {
			synchronized (NetworkData.class) {
				if (mDateFormat == null) {
					mDateFormat = new SimpleDateFormat(mContext.getString(R.string.date_format), Locale.US);
				}
			}
		}
		return mDateFormat;
	}

	public Gson getGson() {
		if (gson == null) {
			synchronized (NetworkData.class) {
				if (gson == null) {
					gson = new GsonBuilder()
							.setDateFormat(this.getDateFormat().toPattern()).create();
				}
			}
		}
		return gson;
	}
}
