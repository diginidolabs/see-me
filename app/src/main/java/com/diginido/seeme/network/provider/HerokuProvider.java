/*
 * Copyright 2015 John Krause
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.network.provider;

import android.content.Context;
import android.net.Uri;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.diginido.seeme.network.data.NetworkData;
import com.diginido.seeme.network.request.heroku.HerokuApiRequest;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author John Krause
 * @since 9/13/15
 */
public class HerokuProvider extends ProviderBase {

	private HerokuApiRequest request;

	private static final int RETRY_TIMEOUT = (int) TimeUnit.SECONDS.toMillis(10L);

	public HerokuProvider(Context context,
						  Response.Listener successListener,
						  Response.ErrorListener errorListener,
						  HerokuApiRequest request) {
		super(context, successListener, errorListener);
		this.request = request;
	}

	@Override
	public void execute(Object tag) {
		execute(getUrl(), tag);
	}

	@Override
	public void execute(String url, Object tag) {
		makeRequest(url, tag, request.getMethod());
	}

	private String getUrl() {
		Uri uri = Uri.parse(NetworkData.getInstance().getHost());
		Uri.Builder builder = uri.buildUpon();
		builder.appendEncodedPath(request.getEndpoint());
		List<Map.Entry<String, Object>> queryParams = request.getQueryParams();
		for (Map.Entry entry : queryParams) {
			builder.appendQueryParameter(entry.getKey().toString(), entry.getValue().toString());
		}
		String url = builder.build().toString();
		return url;
	}

	private void makeRequest(String url, Object tag, int method) {
		Request request = null;
		boolean useCache = true;
		if (method == Request.Method.POST || method == Request.Method.PUT) {
			request = new JsonObjectRequest(method, url,
					this.request.getBody(), mSuccessListener, mErrorListener) {
				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					return mNetRequestQueue.getDefaultRequestHeaders();
				}
			};
			useCache = false;
		} else if (method == Request.Method.GET) {
			request = new StringRequest(Request.Method.GET, url,
					mSuccessListener, mErrorListener) {
				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					return mNetRequestQueue.getDefaultRequestHeaders();
				}
			};
		}
		if (request != null) {
			request.setTag(tag);
			request.setRetryPolicy(new DefaultRetryPolicy(RETRY_TIMEOUT,
					DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
					DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
			mNetRequestQueue.addToRequestQueue(request, mSuccessListener, useCache);
		}
	}
}
