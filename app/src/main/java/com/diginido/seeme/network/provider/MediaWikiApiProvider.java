/*
 * Copyright 2015 John Krause & Shelter Tech
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.network.provider;

import android.content.Context;
import android.net.Uri;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.diginido.seeme.network.data.NetworkData;
import com.diginido.seeme.network.request.mediawiki.MediaWikiApiRequest;

import java.util.Map;

/**
 * @author John Krause on 8/2/15.
 */
public class MediaWikiApiProvider extends ProviderBase {

	private static final String ENDPOINT = "api.php";
	private MediaWikiApiRequest mRequest;

	public MediaWikiApiProvider(Context context, Response.Listener<String> successListener,
								Response.ErrorListener errorListener, MediaWikiApiRequest request) {
		super(context, successListener, errorListener);
		mRequest = request;
	}

	public void fetch(Object tag) {
		Uri uri = Uri.parse(NetworkData.getInstance().getHost());
		Uri.Builder builder = uri.buildUpon();
		builder.appendPath(ENDPOINT);
		Map queryParams = mRequest.getQueryParams();
		for (Object key : queryParams.keySet()) {
			builder.appendQueryParameter(key.toString(), queryParams.get(key).toString());
		}
		String url = builder.build().toString();
		StringRequest request = new StringRequest(Request.Method.GET, url, mSuccessListener, mErrorListener) {
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				return mNetRequestQueue.getDefaultRequestHeaders();
			}
		};
		request.setTag(tag);
		//mNetRequestQueue.addToRequestQueue(request);
	}

	@Override
	public void execute(Object tag) {

	}

	@Override
	public void execute(String url, Object tag) {

	}
}
