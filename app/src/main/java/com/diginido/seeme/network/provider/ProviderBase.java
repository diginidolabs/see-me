/*
 * Copyright 2015 John Krause
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.network.provider;

import android.content.Context;

import com.android.volley.Response;
import com.diginido.seeme.network.NetRequestQueue;

/**
 * @author John Krause on 8/2/15.
 */
public abstract class ProviderBase {

	protected NetRequestQueue mNetRequestQueue;
	protected Response.Listener mSuccessListener;
	protected Response.ErrorListener mErrorListener;
	protected Context mContext;

	public ProviderBase(Context context, Response.Listener successListener,
						Response.ErrorListener errorListener) {
		mContext = context;
		mNetRequestQueue = NetRequestQueue.getInstance(context);
		mSuccessListener = successListener;
		mErrorListener = errorListener;
	}

	public abstract void execute(Object tag);

	public abstract void execute(String url, Object tag);

	public void cancelAllRequests(Object tag) {
		mNetRequestQueue.getRequestQueue().cancelAll(tag);
	}
}
