/*
 * Copyright 2015 John Krause
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.network.request.heroku;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author John Krause on 8/2/15.
 */
public abstract class HerokuApiRequest {

	protected List<Map.Entry<String, Object>> mQueryParams;

	protected HerokuApiRequest() {
		mQueryParams = new ArrayList<>();
	}

	public List<Map.Entry<String, Object>> getQueryParams() {
		return mQueryParams;
	}

	public Object addQueryParam(String key, Object value) {
		return mQueryParams.add(new AbstractMap.SimpleEntry<>(key, value));
	}

	public abstract String getEndpoint();

	public abstract String getBody();

	public abstract int getMethod();
}
