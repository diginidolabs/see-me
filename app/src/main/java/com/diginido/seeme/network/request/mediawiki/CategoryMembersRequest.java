/*
 * Copyright 2015 John Krause & Shelter Tech
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.network.request.mediawiki;

import com.diginido.seeme.network.request.SortBy;
import com.diginido.seeme.network.request.SortDirection;

/**
 * @author John Krause
 * @since 8/2/15
 */
public class CategoryMembersRequest extends MediaWikiApiRequest {

	//https://sfhomeless.wikia.com/api.php?action=query&list=categorymembers&cmlimit=100
	// &cmsort=timestamp&cmpageid=2694&cmprop=ids|title|type|timestamp&cmdir=desc&cmnamespace=0

	//sfhomeless.wikia.com/api.php?action=query&generator=categorymembers&gcmlimit=50&gcmsort=timestamp&gcmpageid=2907&gcmprop=ids&gcmdir=desc&gcmnamespace=0&prop=info|images|revisions&inprop=talkid|created&rvprop=content&imlimit=500&format=jsonfm&rvgeneratexml=t

	public CategoryMembersRequest(int categoryId, int numResultsLimit, SortBy sortBy,
								  SortDirection sortDirection, boolean asGenerator, String startTime) {
		super();
		mAction = Action.query;
		mSortBy = sortBy;
		mSortDirection = sortDirection;
		mQueryParams.put(processKey("cmlimit", asGenerator), String.valueOf(numResultsLimit));
		if (sortBy == SortBy.TIMESTAMP) {
			mQueryParams.put(processKey("cmsort", asGenerator), "timestamp");
		}
		if (startTime != null) {
			mQueryParams.put(processKey("cmstart", asGenerator), startTime);
		}
		mQueryParams.put(processKey("cmpageid", asGenerator), String.valueOf(categoryId));
		mQueryParams.put(processKey("cmprops", asGenerator), "ids|title|timestamp");
		if (sortDirection == SortDirection.DESCENDING) {
			mQueryParams.put(processKey("cmdir", asGenerator), "desc");
		}
		mQueryParams.put(processKey("cmnamespace", asGenerator), "0");
		if (asGenerator) {
			mQueryParams.put("generator", "categorymembers");
		} else {
			mQueryParams.put("list", "categorymembers");
		}
	}


}
