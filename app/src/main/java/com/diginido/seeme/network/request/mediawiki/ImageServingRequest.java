/*
 * Copyright 2015 John Krause & Shelter Tech
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.network.request.mediawiki;

import android.util.Log;

import com.diginido.seeme.Constants;

/**
 * @author John Krause
 * @since 8/7/15
 */
public class ImageServingRequest extends MediaWikiApiRequest {

	public ImageServingRequest(String imageTitle, Integer imageId) {
		super();
		mAction = Action.imageserving;
		mSortBy = null;
		mSortDirection = null;
		if (imageTitle != null) {
			mQueryParams.put("wisTitle", imageTitle);
		} else if (imageId != null) {
			mQueryParams.put("wisId", imageId);
		} else {
			Log.e(Constants.getLogTag(this), "Image ID or Image Title must not be null");
		}
	}

}
