/*
 * Copyright 2015 John Krause & Shelter Tech
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.network.request.mediawiki;

import com.diginido.seeme.network.request.SortBy;
import com.diginido.seeme.network.request.SortDirection;

import java.util.HashMap;
import java.util.Map;

/**
 * @author John Krause on 8/2/15.
 */
public abstract class MediaWikiApiRequest {

	/**
	 * Different actions available on mediawiki api
	 */
	protected enum Action {
		query,
		geocode,
		infobox,
		login,
		logout,
		edit,
		upload,
		emailuser,
		watch,
		patrol,
		options,
		imageserving
	}

	protected Map<String, Object> mQueryParams;
	protected Action mAction;
	protected SortBy mSortBy;
	protected SortDirection mSortDirection;

	protected MediaWikiApiRequest() {
		mQueryParams = new HashMap<>();
		mQueryParams.put("format", "json");
		mQueryParams.put("formatversion", "2");
		mQueryParams.put("rawcontinue", "t");
		mQueryParams.put("redirects", "t");
	}

	public Map<String, Object> getQueryParams() {
		mQueryParams.put("action", mAction.name());
		return mQueryParams;
	}

	protected String processKey(String key, boolean asGenerator) {
		return asGenerator ? "g" + key : key;
	}

	public Object setQueryParam(String key, Object value) {
		return mQueryParams.put(key, value);
	}

	public SortBy getSortBy() {
		return mSortBy;
	}

	public SortDirection getSortDirection() {
		return mSortDirection;
	}
}
