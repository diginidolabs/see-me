/*
 * Copyright 2015 John Krause & Shelter Tech
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.network.request.mediawiki;

/**
 * @author John Krause
 * @since 8/2/15
 */
public class PageRequest extends MediaWikiApiRequest {

	//https://sfhomeless.wikia.com/api.php?action=query&prop=info|images|revisions&inprop=talkid|created&rvprop=content&imlimit=100&rvgeneratexml=t&titles=Main%20Page&redirects=t&format=jsonfm

	public PageRequest(Integer pageId, String pageTitle, int imagesLimit) {
		super();
		mAction = Action.query;
		mQueryParams.put("prop", "info|images|revisions");
		mQueryParams.put("inprop", "talkid|created");
		mQueryParams.put("rvprop", "content");
		mQueryParams.put("imlimit", imagesLimit);
		mQueryParams.put("rvgeneratexml", "t");
		if (pageId != null) {
			mQueryParams.put("pageids", pageId);
		} else {
			mQueryParams.put("titles", pageTitle);
		}
	}


}
