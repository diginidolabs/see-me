/*
 * Copyright 2015 John Krause
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.network.response.model;

import java.io.Serializable;
import java.util.List;

/**
 * @author John Krause
 * @since 8/3/15
 */
public interface Page extends Serializable {

	int getId();

	int getPageId();

	boolean hasReviews();

	String getTitle();

	String getEmail();

	String getWebsite();

	String getContent();

	String getSummary();

	List<Category> getCategories();

	List<ResourceImage> getResourceImages();

	List<PhoneNumber> getPhoneNumbers();

	List<Address> getAddresses();

	RatingCounts getRatingCounts();

	Rating getMyRating();
}
