/*
 * Copyright 2015 John Krause
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.network.response.model.herokudb;

import com.diginido.seeme.network.response.model.Address;

/**
 * @author John Krause
 * @since 9/13/15
 */
public class AddressImpl implements Address {

	private int id;
	private String full_street_address, city, state, state_code, country, country_code;
	private float longitude, latitude;

	@Override
	public int getId() {
		return id;
	}

	@Override
	public String getFullStreetAddress() {
		return full_street_address;
	}

	@Override
	public String getCity() {
		return city;
	}

	@Override
	public String getState() {
		return state;
	}

	@Override
	public String getStateCode() {
		return state_code;
	}

	@Override
	public String getCountry() {
		return country;
	}

	@Override
	public String getCountryCode() {
		return country_code;
	}

	@Override
	public float getLongitude() {
		return longitude;
	}

	@Override
	public float getLatitude() {
		return latitude;
	}
}
