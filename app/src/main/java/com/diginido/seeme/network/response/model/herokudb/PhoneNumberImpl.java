/*
 * Copyright 2015 John Krause
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.network.response.model.herokudb;

import com.diginido.legacy.appseeme.R;
import com.diginido.seeme.SeeMeApplication;
import com.diginido.seeme.network.response.model.PhoneNumber;


/**
 * @author John Krause
 * @since 9/13/15
 */
public class PhoneNumberImpl implements PhoneNumber {

	private int id, country_code, area_code, number, extension;
	private String comment;

	@Override
	public int getId() {
		return id;
	}

	@Override
	public int getCountryCode() {
		return country_code;
	}

	@Override
	public int getAreaCode() {
		return area_code;
	}

	@Override
	public int getNumber() {
		return number;
	}

	@Override
	public int getExtension() {
		return extension;
	}

	@Override
	public String getComment() {
		return comment;
	}

	@Override
	public String toString() {
		return String.format(SeeMeApplication.getAppContext()
						.getString(R.string.phone_number_format),
				String.valueOf(country_code), String.valueOf(area_code), String.valueOf(number));
	}
}
