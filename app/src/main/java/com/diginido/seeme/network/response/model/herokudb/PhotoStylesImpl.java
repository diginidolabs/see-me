/*
 * Copyright 2015 John Krause
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.network.response.model.herokudb;

import com.diginido.seeme.network.response.model.PhotoStyles;

/**
 * @author John Krause
 * @since 9/13/15
 */
public class PhotoStylesImpl implements PhotoStyles {

	private String thumbnail, medium, large;

	@Override
	public String getThumbnail() {
		return thumbnail;
	}

	@Override
	public String getMedium() {
		return medium;
	}

	@Override
	public String getLarge() {
		return large;
	}
}
