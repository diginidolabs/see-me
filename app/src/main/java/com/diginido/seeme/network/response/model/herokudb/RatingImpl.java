/*
 * Copyright 2015 John Krause
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.network.response.model.herokudb;

import com.diginido.seeme.network.response.model.Rating;

/**
 * @author John Krause
 * @since 10/4/15
 */
public class RatingImpl implements Rating {

	int id, resource_id;
	String device_id, rating_option_name;

	@Override
	public int getId() {
		return id;
	}

	@Override
	public int getResourceId() {
		return resource_id;
	}

	@Override
	public String getDeviceId() {
		return device_id;
	}

	@Override
	public String getRatingOptionName() {
		return rating_option_name;
	}

	public void setResource_id(int resource_id) {
		this.resource_id = resource_id;
	}

	public void setDevice_id(String device_id) {
		this.device_id = device_id;
	}

	public void setRating_option_name(String rating_option_name) {
		this.rating_option_name = rating_option_name;
	}
}
