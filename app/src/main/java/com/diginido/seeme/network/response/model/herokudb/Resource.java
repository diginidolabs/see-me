/*
 * Copyright 2015 John Krause
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.network.response.model.herokudb;

import com.diginido.seeme.network.response.model.Address;
import com.diginido.seeme.network.response.model.Category;
import com.diginido.seeme.network.response.model.Page;
import com.diginido.seeme.network.response.model.PhoneNumber;
import com.diginido.seeme.network.response.model.Rating;
import com.diginido.seeme.network.response.model.RatingCounts;
import com.diginido.seeme.network.response.model.ResourceImage;

import java.util.Arrays;
import java.util.List;

/**
 * @author John Krause
 * @since 9/13/15
 */
public class Resource implements Page {

	int id, page_id;
	String title, email, website, summary, content;
	CategoryImpl[] categories;
	PhoneNumberImpl[] phone_numbers;
	AddressImpl[] addresses;
	ResourceImageImpl[] resource_images;
	RatingCountsImpl rating_counts;
	RatingImpl my_rating;

	@Override
	public int getId() {
		return id;
	}

	@Override
	public int getPageId() {
		return page_id;
	}

	@Override
	public boolean hasReviews() {
		return false;
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public String getEmail() {
		return email;
	}

	@Override
	public String getWebsite() {
		return website;
	}

	@Override
	public String getContent() {
		return content;
	}

	@Override
	public String getSummary() {
		return summary;
	}

	@Override
	public List<Category> getCategories() {
		return Arrays.asList((Category[]) categories);
	}

	@Override
	public List<ResourceImage> getResourceImages() {
		return Arrays.asList((ResourceImage[]) resource_images);
	}

	@Override
	public List<PhoneNumber> getPhoneNumbers() {
		return Arrays.asList((PhoneNumber[]) phone_numbers);
	}

	@Override
	public List<Address> getAddresses() {
		return Arrays.asList((Address[]) addresses);
	}

	@Override
	public RatingCounts getRatingCounts() {
		return rating_counts;
	}

	@Override
	public Rating getMyRating() {
		return my_rating;
	}

	public void setMy_rating(RatingImpl my_rating) {
		this.my_rating = my_rating;
	}
}
