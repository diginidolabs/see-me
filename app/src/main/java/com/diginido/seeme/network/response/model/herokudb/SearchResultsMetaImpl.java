/*
 * Copyright 2015 John Krause
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.network.response.model.herokudb;

import com.diginido.seeme.network.response.model.SearchResultsMeta;

/**
 * @author John Krause
 * @since 9/13/15
 */
public class SearchResultsMetaImpl implements SearchResultsMeta {

	private int current_page, next_page, prev_page, total_pages, total_count;
	private String self, next_url, prev_url;

	@Override
	public int getCurrentPage() {
		return current_page;
	}

	@Override
	public int getNextPage() {
		return next_page;
	}

	@Override
	public int getPrevPage() {
		return prev_page;
	}

	@Override
	public int getTotalPages() {
		return total_pages;
	}

	@Override
	public int getTotalCount() {
		return total_count;
	}

	@Override
	public String getCurrentUrl() {
		return self;
	}

	@Override
	public String getNextUrl() {
		return next_url;
	}

	@Override
	public String getPrevUrl() {
		return prev_url;
	}
}
