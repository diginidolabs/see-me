/*
 * Copyright 2015 John Krause & Shelter Tech
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.network.response.model.mediawiki;

import java.io.Serializable;
import java.util.Date;

/**
 * @author John Krause
 * @since 8/2/15
 */
public class CategoryMember implements Serializable {
	private int pageid, ns;
	private String title, type, sortkey, sortkeyprefix;
	private Date timestamp;

	public int getPageid() {
		return pageid;
	}

	public int getNs() {
		return ns;
	}

	public String getTitle() {
		return title;
	}

	public String getType() {
		return type;
	}

	public String getSortkey() {
		return sortkey;
	}

	public String getSortkeyprefix() {
		return sortkeyprefix;
	}

	public Date getTimestamp() {
		return timestamp;
	}
}
