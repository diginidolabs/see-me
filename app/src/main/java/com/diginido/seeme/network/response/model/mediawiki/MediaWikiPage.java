/*
 * Copyright 2015 John Krause & Shelter Tech
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.network.response.model.mediawiki;

import com.diginido.seeme.model.PageContent;
import com.diginido.seeme.network.response.model.Address;
import com.diginido.seeme.network.response.model.Category;
import com.diginido.seeme.network.response.model.Page;
import com.diginido.seeme.network.response.model.PhoneNumber;
import com.diginido.seeme.network.response.model.Rating;
import com.diginido.seeme.network.response.model.RatingCounts;
import com.diginido.seeme.network.response.model.ResourceImage;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author John Krause
 * @since 8/3/15
 */
public class MediaWikiPage implements Page {
	private Integer pageid, ns, lastrevid, length, talkid;
	private String title, counter;
	private Date created, touched;
	private Images[] images;
	private Revisions[] revisions;
	private MediaWikiPage talkPage;
	private PageContent pageContent;
	private Location location;

	@Override
	public String toString() {
		return "Page{" +
				"pageid=" + pageid +
				", ns=" + ns +
				", lastrevid=" + lastrevid +
				", length=" + length +
				", talkid=" + talkid +
				", title='" + title + '\'' +
				", counter='" + counter + '\'' +
				", created=" + created +
				", touched=" + touched +
				", images=" + Arrays.toString(images) +
				", revisions=" + Arrays.toString(revisions) +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		MediaWikiPage page = (MediaWikiPage) o;

		return pageid.equals(page.pageid);

	}

	@Override
	public int hashCode() {
		return pageid;
	}

	public Integer getPageid() {
		return pageid;
	}

	public Integer getNs() {
		return ns;
	}

	public Integer getLastrevid() {
		return lastrevid;
	}

	public Integer getLength() {
		return length;
	}

	public Integer getTalkid() {
		return talkid;
	}

	public String getTitle() {
		return title;
	}

	public String getCounter() {
		return counter;
	}

	public Date getCreated() {
		return created;
	}

	public Date getTouched() {
		return touched;
	}

	public Images[] getImages() {
		return images;
	}

	public Revisions[] getRevisions() {
		return revisions;
	}

	public MediaWikiPage getTalkPage() {
		return talkPage;
	}

	public void setTalkPage(MediaWikiPage talkPage) {
		this.talkPage = talkPage;
	}

	public PageContent getPageContent() {
		return pageContent;
	}

	public void setPageContent(PageContent pageContent) {
		this.pageContent = pageContent;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	//TODO make this work right
	@Override
	public int getPageId() {
		return 0;
	}

	@Override
	public boolean hasReviews() {
		return false;
	}

	@Override
	public String getEmail() {
		return null;
	}

	@Override
	public String getWebsite() {
		return null;
	}

	@Override
	public String getContent() {
		return null;
	}

	@Override
	public String getSummary() {
		return null;
	}

	@Override
	public List<Category> getCategories() {
		return null;
	}

	@Override
	public List<ResourceImage> getResourceImages() {
		return null;
	}

	@Override
	public List<PhoneNumber> getPhoneNumbers() {
		return null;
	}

	@Override
	public List<Address> getAddresses() {
		return null;
	}

	@Override
	public RatingCounts getRatingCounts() {
		return null;
	}

	@Override
	public int getId() {
		return 0;
	}

	@Override
	public Rating getMyRating() {
		return null;
	}
}
