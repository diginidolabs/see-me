/*
 * Copyright 2015 John Krause
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.network.service;

import android.content.Context;
import android.graphics.Bitmap;

import com.diginido.seeme.network.response.model.ResourceImage;

/**
 * @author John Krause
 * @since 8/7/15
 */
public interface ImageService {

	enum ImageSize {
		THUMBNAIL, MEDIUM, LARGE, ORIGINAL;

		public static ImageSize getSmallest() {
			return THUMBNAIL;
		}

		public static ImageSize getNextSmaller(ImageSize imageSize) {
			for (ImageSize size : ImageSize.values()) {
				if (size.ordinal() == imageSize.ordinal() - 1) {
					return size;
				}
			}
			return THUMBNAIL;
		}
	}

	interface Listener {
		void onSuccess(Bitmap image);

		void onFailure(Exception e);
	}

	void getImage(final ResourceImage image, ImageSize size,
				  Context context, final Listener listener, Object tag);

	void cancel(Object tag);
}
