/*
 * Copyright 2015 John Krause
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.network.service;

import android.content.Context;

import com.diginido.seeme.network.request.SortBy;
import com.diginido.seeme.network.request.SortDirection;
import com.diginido.seeme.network.response.model.Category;
import com.diginido.seeme.network.response.model.Page;
import com.diginido.seeme.network.response.model.SearchResultsMeta;

import java.util.List;

/**
 * @author John Krause
 * @since 8/3/15
 */
public interface PageService {

	public interface Listener {
		void onSuccess(List<Page> pages, final SearchResultsMeta continueRequest);

		void onFailure(Exception e);
	}

	public void getPagesInCategories(Context context, List<Category> categories, SortBy sortBy,
									 SortDirection sortDirection, final Listener listener,
									 Object tag);

	public void getNextSetOfResults(SearchResultsMeta meta, Context context, Listener listener, Object tag);

	public void cancel(Object tag);
}
