/*
 * Copyright 2015 John Krause
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.network.service.heroku;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.diginido.seeme.Constants;
import com.diginido.seeme.network.NetRequestQueue;
import com.diginido.seeme.network.response.model.ResourceImage;
import com.diginido.seeme.network.service.ImageService;

import java.util.ArrayList;
import java.util.List;

/**
 * @author John Krause
 * @since 8/7/15
 */
public class HerokuImageService implements ImageService {

	private class ImageTracker {
		ImageLoader.ImageContainer imageContainer;
		Object tag;
	}

	private List<ImageTracker> imageTrackers;
	private static final int CONTAINER_LIST_SIZE = 10;

	public HerokuImageService() {
		imageTrackers = new ArrayList<>(CONTAINER_LIST_SIZE);
	}

	public interface Listener {
		void onSuccess(Bitmap image);

		void onFailure(Exception e);
	}

	@Override
	public void getImage(ResourceImage image, ImageSize size,
						 Context context, final ImageService.Listener listener, Object tag) {
		if (image != null) {
			fetchImage(context, image, size, listener, tag);
		} else {
			listener.onFailure(new Exception("Image given is null"));
		}
	}

	@Override
	public void cancel(Object tag) {
		for (ImageTracker tracker : imageTrackers) {
			if (tracker.tag.equals(tag)) {
				tracker.imageContainer.cancelRequest();
			}
		}
	}

	private void fetchImage(final Context context, final ResourceImage image,
							final ImageSize size, final ImageService.Listener listener,
							final Object tag) {
		final ImageTracker tracker = new ImageTracker();
		tracker.tag = tag;
		ImageLoader imageLoader = NetRequestQueue.getInstance(context).getImageLoader();
		tracker.imageContainer = imageLoader.get(getImageUrl(image, size),
				new ImageLoader.ImageListener() {
					@Override
					public void onResponse(ImageLoader.ImageContainer image, boolean isImmediate) {
						if (image.getBitmap() != null) {
							listener.onSuccess(image.getBitmap());
						} else {
							listener.onFailure(new Exception("Bitmap returned from service was null"));
						}
						imageTrackers.remove(tracker);
					}

					@Override
					public void onErrorResponse(VolleyError error) {
						Log.e(Constants.getLogTag(this), "Error getting image", error);
						if (!size.equals(ImageSize.getSmallest())) {
							fetchImage(context, image, ImageSize.getNextSmaller(size), listener, tag);
						} else {
							listener.onFailure(error);
						}
						imageTrackers.remove(tracker);
					}
				});
		imageTrackers.add(tracker);
	}

	private String getImageUrl(ResourceImage image, ImageSize size) {
		String imageUrl;
		switch (size) {
			case THUMBNAIL:
				imageUrl = image.getPhotoStyles().getThumbnail();
				break;
			case MEDIUM:
				imageUrl = image.getPhotoStyles().getMedium();
				break;
			case LARGE:
				imageUrl = image.getPhotoStyles().getLarge();
				break;
			case ORIGINAL:
				imageUrl = image.getPhotoUrl();
				break;
			default:
				imageUrl = image.getPhotoStyles().getThumbnail();
				break;
		}
		return imageUrl;
	}
}
