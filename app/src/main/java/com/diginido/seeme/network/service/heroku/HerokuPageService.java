/*
 * Copyright 2015 John Krause
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.network.service.heroku;

import android.content.Context;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.diginido.seeme.Constants;
import com.diginido.seeme.network.data.NetworkData;
import com.diginido.seeme.network.provider.HerokuProvider;
import com.diginido.seeme.network.request.SortBy;
import com.diginido.seeme.network.request.SortDirection;
import com.diginido.seeme.network.request.heroku.HerokuApiRequest;
import com.diginido.seeme.network.request.heroku.ResourcesRequest;
import com.diginido.seeme.network.response.model.Category;
import com.diginido.seeme.network.response.model.Page;
import com.diginido.seeme.network.response.model.SearchResultsMeta;
import com.diginido.seeme.network.response.model.herokudb.Resource;
import com.diginido.seeme.network.response.model.herokudb.SearchResultsMetaImpl;
import com.diginido.seeme.network.service.PageService;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

/**
 * @author John Krause
 * @since 9/13/15
 */
public class HerokuPageService implements PageService {

	private HerokuProvider herokuProvider;
	private static final int RESULTS_PER_REQUEST = 50;

	@Override
	public void getPagesInCategories(Context context,
									 List<Category> categories,
									 SortBy sortBy, SortDirection sortDirection,
									 Listener listener, Object tag) {
		HerokuApiRequest request = new ResourcesRequest();
		for (Category category : categories) {
			request.addQueryParam("category_ids[]", category.getId());
		}
		makeRequest(request, context, listener, tag);
	}

	@Override
	public void getNextSetOfResults(SearchResultsMeta meta, Context context,
									final Listener listener, Object tag) {
		herokuProvider = new HerokuProvider(context, new Response.Listener<String>() {
			@Override
			public void onResponse(String response) {
				listener.onSuccess(getPagesFromResponse(response),
						getSearchResultsMetaFromResponse(response));
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				listener.onFailure(error);
			}
		}, new ResourcesRequest());
		//TODO this happens twice for some reason
		herokuProvider.execute(meta.getNextUrl(), tag);
	}

	private void makeRequest(HerokuApiRequest request,
							 Context context, final Listener listener, Object tag) {
		herokuProvider = new HerokuProvider(context, new Response.Listener<String>() {
			@Override
			public void onResponse(String response) {
				listener.onSuccess(getPagesFromResponse(response),
						getSearchResultsMetaFromResponse(response));
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				listener.onFailure(error);
			}
		}, request);
		//TODO this happens twice for some reason
		herokuProvider.execute(tag);
	}

	private List<Page> getPagesFromResponse(String response) {
		List<Page> pages = new ArrayList<>(RESULTS_PER_REQUEST);
		final Gson gson = NetworkData.getInstance().getGson();
		try {
			JsonArray jpages = gson.fromJson(response, JsonObject.class)
					.getAsJsonArray("resources");
			for (JsonElement element : jpages) {
				pages.add(gson.fromJson(element, Resource.class));
			}
		} catch (ClassCastException e) {
			Log.e(Constants.getLogTag(this),
					String.format("Problem getting page from response %s", response), e);
		}
		return pages;
	}

	private SearchResultsMeta getSearchResultsMetaFromResponse(String response) {
		SearchResultsMeta meta = null;
		final Gson gson = NetworkData.getInstance().getGson();
		try {
			JsonObject jmeta = gson.fromJson(response, JsonObject.class)
					.getAsJsonObject("meta");
			meta = gson.fromJson(jmeta, SearchResultsMetaImpl.class);
		} catch (ClassCastException e) {
			Log.e(Constants.getLogTag(this),
					String.format("Problem getting meta from response %s", response), e);
		}
		return meta;
	}

	@Override
	public void cancel(Object tag) {
		if (herokuProvider != null) {
			herokuProvider.cancelAllRequests(tag);
		}
	}
}
