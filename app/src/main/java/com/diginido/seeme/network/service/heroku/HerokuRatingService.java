/*
 * Copyright 2015 John Krause
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.network.service.heroku;

import android.content.Context;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.diginido.seeme.network.data.NetworkData;
import com.diginido.seeme.network.provider.HerokuProvider;
import com.diginido.seeme.network.request.heroku.RatingsRequest;
import com.diginido.seeme.network.response.model.Page;
import com.diginido.seeme.network.response.model.Rating;
import com.diginido.seeme.network.response.model.herokudb.RatingCountsImpl;
import com.diginido.seeme.network.response.model.herokudb.RatingImpl;
import com.diginido.seeme.network.response.model.herokudb.Resource;
import com.diginido.seeme.network.service.RatingService;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import ly.count.android.sdk.OpenUDIDAdapter;

/**
 * @author John Krause
 * @since 10/4/15
 */
public class HerokuRatingService implements RatingService {

	private HerokuProvider herokuProvider;

	@Override
	public void updateRating(Context context, final Page page, final Rating.RatingOptions ratingOption,
							 final Listener listener, Object tag) {
		herokuProvider = new HerokuProvider(context, new Response.Listener() {
			@Override
			public void onResponse(Object response) {
				RatingCountsImpl ratingCounts = ((RatingCountsImpl) page.getRatingCounts());
				RatingImpl rating = ((RatingImpl) page.getMyRating());
				JSONObject jsonObject = (JSONObject) response;
				if (rating != null) {
					if (!rating.getRatingOptionName()
							.equalsIgnoreCase(ratingOption.toString())) {
						ratingCounts.decrementRating(Rating.RatingOptions
								.valueOf(rating.getRatingOptionName().toUpperCase()));
						ratingCounts.incrementRating(ratingOption);
					}
				} else {
					ratingCounts.incrementRating(ratingOption);
				}
				try {
					((Resource) page).setMy_rating(NetworkData.getInstance().getGson()
							.fromJson(jsonObject.getJSONObject("rating").toString(), RatingImpl.class));
					listener.onSuccess();
				} catch (JSONException e) {
					listener.onFailure(e);
				}
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				listener.onFailure(error);
			}
		}, new RatingsRequest(getBody(page, ratingOption), page.getMyRating()));
		herokuProvider.execute(tag);
	}

	private String getBody(Page page, Rating.RatingOptions ratingOption) {
		RatingImpl rating = new RatingImpl();
		rating.setDevice_id(OpenUDIDAdapter.getOpenUDID());
		rating.setResource_id(page.getId());
		rating.setRating_option_name(ratingOption.toString().toLowerCase());
		Gson gson = NetworkData.getInstance().getGson();
		JsonObject jsonObject = new JsonObject();
		jsonObject.add("rating", gson.toJsonTree(rating));
		return gson.toJson(jsonObject);
	}

	@Override
	public void cancel(Object tag) {
		if (herokuProvider != null) {
			herokuProvider.cancelAllRequests(tag);
		}
	}
}
