/*
 * Copyright 2015 John Krause & Shelter Tech
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.network.service.mediawiki;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.diginido.seeme.Constants;
import com.diginido.seeme.network.NetRequestQueue;
import com.diginido.seeme.network.data.NetworkData;
import com.diginido.seeme.network.provider.MediaWikiApiProvider;
import com.diginido.seeme.network.request.mediawiki.ImageServingRequest;
import com.diginido.seeme.network.response.model.mediawiki.Image;
import com.diginido.seeme.network.response.model.mediawiki.Images;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

/**
 * @author John Krause
 * @since 8/7/15
 */
public class MediaWikiImageService extends MediaWikiServiceBase {

	private List<ImageLoader.ImageContainer> imageContainers;
	private static final int CONTAINER_LIST_SIZE = 10;

	public MediaWikiImageService() {
		imageContainers = new ArrayList<>(CONTAINER_LIST_SIZE);
	}

	public interface Listener {
		void onSuccess(Bitmap image);

		void onFailure(Exception e);
	}

	public void getImage(final Image image, Context context, final Listener listener) {
		if (image != null) {
			ImageLoader imageLoader = NetRequestQueue.getInstance(context).getImageLoader();
			imageContainers.add(imageLoader.get(image.getImageserving(), new ImageLoader.ImageListener() {
				@Override
				public void onResponse(ImageLoader.ImageContainer image, boolean isImmediate) {
					imageContainers.remove(image);
					listener.onSuccess(image.getBitmap());
				}

				@Override
				public void onErrorResponse(VolleyError error) {
					Log.e(Constants.getLogTag(this), "Error getting image", error);
					listener.onFailure(error);
				}
			}));
		} else {
			listener.onFailure(new Exception("Image given is null"));
		}
	}

	public void getImage(final Images images, final Context context, final Listener listener, Object tag) {
		if (images != null) {
			if (images.getImage() == null) {
				ImageServingRequest request = new ImageServingRequest(images.getTitle(), null);
				mediaWikiApiProvider = new MediaWikiApiProvider(context,
						new Response.Listener<String>() {
							@Override
							public void onResponse(String response) {
								final Image image = getImageFromResponse(response);
								images.setImage(image);
								getImage(image, context, listener);
							}
						}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						Log.e(Constants.getLogTag(this), "Error getting image", error);
						listener.onFailure(error);
					}
				}, request);
				mediaWikiApiProvider.fetch(tag);
			} else {
				getImage(images.getImage(), context, listener);
			}
		} else {
			listener.onFailure(new Exception("Images given is null"));
		}
	}

	private Image getImageFromResponse(String response) {
		Gson gson = NetworkData.getInstance().getGson();
		try {
			JsonObject jresponse = gson.fromJson(response, JsonObject.class);
			JsonObject jimage = jresponse.getAsJsonObject().getAsJsonObject("image");
			return gson.fromJson(jimage, Image.class);
		} catch (ClassCastException e) {
			Log.e(Constants.getLogTag(this),
					String.format("Problem getting image from response %s", response), e);
			return null;
		}
	}

	@Override
	public void cancel(Object tag) {
		super.cancel(tag);
		for (ImageLoader.ImageContainer container : imageContainers) {
			container.cancelRequest();
		}
	}
}
