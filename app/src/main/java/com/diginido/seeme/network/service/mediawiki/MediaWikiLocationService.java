/*
 * Copyright 2015 John Krause & Shelter Tech
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.network.service.mediawiki;

import android.content.Context;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.diginido.seeme.Constants;
import com.diginido.seeme.model.PageContent;
import com.diginido.seeme.network.data.NetworkData;
import com.diginido.seeme.network.provider.MediaWikiApiProvider;
import com.diginido.seeme.network.request.mediawiki.LocationRequest;
import com.diginido.seeme.network.response.model.mediawiki.Location;
import com.diginido.seeme.network.response.model.mediawiki.MediaWikiPage;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 * @author John Krause
 * @since 8/16/15
 */
public class MediaWikiLocationService extends MediaWikiServiceBase {

	public interface Listener {
		void onSuccess(Location location);

		void onFailure(Exception e);
	}

	public void getLocation(final MediaWikiPage page, Context context, final Listener listener, Object tag) {
		if (page != null) {
			LocationRequest request;
			PageContent pageContent = page.getPageContent();
			if (pageContent != null && pageContent.getAddress() != null) {
				request = new LocationRequest(pageContent.getAddress());
			} else {
				request = new LocationRequest(page.getTitle());
			}
			mediaWikiApiProvider = new MediaWikiApiProvider(context,
					new Response.Listener<String>() {
						@Override
						public void onResponse(String response) {
							Location location = getLocationFromResponse(response);
							page.setLocation(location);
							listener.onSuccess(location);
						}
					},
					new Response.ErrorListener() {
						@Override
						public void onErrorResponse(VolleyError error) {
							Log.e(Constants.getLogTag(this), "Error getting location", error);
							listener.onFailure(error);
						}
					}, request);
			mediaWikiApiProvider.fetch(tag);
		} else {
			listener.onFailure(new Exception("Page given is null"));
		}
	}

	private Location getLocationFromResponse(String response) {
		Gson gson = NetworkData.getInstance().getGson();
		try {
			JsonObject root = gson.fromJson(response, JsonObject.class);
			JsonObject results = root.getAsJsonObject("results");
			JsonObject query = results.entrySet().iterator().next().getValue().getAsJsonObject();
			JsonArray locations = query.getAsJsonArray("locations");
			if (locations.size() > 0) {
				JsonObject location = locations.get(0).getAsJsonObject();
				return gson.fromJson(location, Location.class);
			}
		} catch (ClassCastException e) {
			Log.e(Constants.getLogTag(this),
					String.format("Error getting location from response %s", response), e);
		}
		return null;
	}
}
