/*
 * Copyright 2015 John Krause & Shelter Tech
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.network.service.mediawiki;

import android.content.Context;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.diginido.seeme.Constants;
import com.diginido.seeme.network.data.NetworkData;
import com.diginido.seeme.network.provider.MediaWikiApiProvider;
import com.diginido.seeme.network.request.SortBy;
import com.diginido.seeme.network.request.SortDirection;
import com.diginido.seeme.network.request.mediawiki.CategoryMembersRequest;
import com.diginido.seeme.network.request.mediawiki.MediaWikiApiRequest;
import com.diginido.seeme.network.request.mediawiki.PageRequest;
import com.diginido.seeme.network.response.model.Page;
import com.diginido.seeme.network.response.model.mediawiki.MediaWikiPage;
import com.diginido.seeme.utils.RevisionContentParser;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * @author John Krause
 * @since 8/3/15
 */
public class MediaWikiPageService extends MediaWikiServiceBase {

	private static final int RESULTS_PER_REQUEST = 50,
			IMAGES_LIMIT = 500;

	public interface Listener {
		void onSuccess(List<Page> pages, final MediaWikiApiRequest continueRequest);

		void onFailure(Exception e);
	}

	public void getPagesInCategory(Context context, int categoryId, SortBy sortBy,
								   SortDirection sortDirection, final Listener listener,
								   Object tag) {
		CategoryMembersRequest request = new CategoryMembersRequest(categoryId, RESULTS_PER_REQUEST,
				sortBy, sortDirection, true, null);
		request.setQueryParam("prop", "info|images|revisions");
		request.setQueryParam("inprop", "talkid|created");
		request.setQueryParam("rvprop", "content");
		request.setQueryParam("imlimit", IMAGES_LIMIT);
		request.setQueryParam("rvgeneratexml", "t");
		makeRequest(request, context, listener, tag);
	}

	public void getPage(Context context, Integer pageId, String pageTitle, Listener listener, Object tag) {
		PageRequest pageRequest = new PageRequest(pageId, pageTitle, IMAGES_LIMIT);
		makeRequest(pageRequest, context, listener, tag);
	}

	public void getNextSetOfResults(MediaWikiApiRequest continueRequest, Context context, Listener listener, Object tag) {
		makeRequest(continueRequest, context, listener, tag);
	}

	private void makeRequest(final MediaWikiApiRequest request,
							 final Context context,
							 final Listener listener, final Object tag) {
		mediaWikiApiProvider = new MediaWikiApiProvider(context,
				new Response.Listener<String>() {
					@Override
					public void onResponse(String response) {
						final List<MediaWikiPage> pages = getPagesFromResponse(response, request);
						getPageContent(pages);
						getTalkPages(pages, context, tag);
						listener.onSuccess(wrapPages(pages), processQueryContinue(response, request));
					}
				}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				Log.e(Constants.getLogTag(this), "Error getting pages", error);
				listener.onFailure(error);
			}
		}, request);
		mediaWikiApiProvider.fetch(tag);
	}

	private List<Page> wrapPages(List<MediaWikiPage> pages) {
		List<Page> pageList = new ArrayList<>(pages.size());
		for (MediaWikiPage page : pages) {
			pageList.add(page);
		}
		return pageList;
	}

	private List<MediaWikiPage> getPagesFromResponse(String response, MediaWikiApiRequest request) {
		List<MediaWikiPage> pages = new ArrayList<>(RESULTS_PER_REQUEST);
		final Gson gson = NetworkData.getInstance().getGson();
		try {
			JsonObject jpages = gson.fromJson(response, JsonObject.class)
					.getAsJsonObject("query")
					.getAsJsonObject("pages");
			for (Map.Entry<String, JsonElement> entry : jpages.entrySet()) {
				JsonElement element = entry.getValue();
				pages.add(gson.fromJson(element, MediaWikiPage.class));
			}
			if (request.getSortBy() != null) {
				switch (request.getSortBy()) {
					case TIMESTAMP:
						pages = sortPagesChronologically(pages, request.getSortDirection());
						break;
					case TITLE:
						pages = sortPagesAlphabetically(pages, request.getSortDirection());
						break;
				}
			}
		} catch (ClassCastException e) {
			Log.e(Constants.getLogTag(this),
					String.format("Problem getting page from response %s", response), e);
		}
		return pages;
	}

	private List<MediaWikiPage> sortPagesChronologically(List<MediaWikiPage> pages,
														 final SortDirection direction) {
		Collections.sort(pages, new Comparator<MediaWikiPage>() {
			@Override
			public int compare(MediaWikiPage lhs, MediaWikiPage rhs) {
				return lhs.getTouched().compareTo(rhs.getTouched());
			}
		});
		if (direction == SortDirection.DESCENDING) {
			Collections.reverse(pages);
		}
		return pages;
	}

	private List<MediaWikiPage> sortPagesAlphabetically(List<MediaWikiPage> pages,
														SortDirection direction) {
		Collections.sort(pages, new Comparator<MediaWikiPage>() {
			@Override
			public int compare(MediaWikiPage lhs, MediaWikiPage rhs) {
				return lhs.getTitle().compareTo(rhs.getTitle());
			}
		});
		if (direction == SortDirection.DESCENDING) {
			Collections.reverse(pages);
		}
		return pages;

	}

	private void getPageContent(List<MediaWikiPage> pages) {
		RevisionContentParser parser = RevisionContentParser.getInstance();
		for (MediaWikiPage page : pages) {
			page.setPageContent(parser.parsePage(page));
		}
	}

	private void getTalkPages(final List<MediaWikiPage> retrievedPages, Context context, Object tag) {
		for (final MediaWikiPage page : retrievedPages) {
			if (page.getTalkid() != null) {
				getPage(context, page.getTalkid(), null, new Listener() {
					@Override
					public void onSuccess(List<Page> pages, MediaWikiApiRequest continueRequest) {
						if (pages.size() > 0) {
							page.setTalkPage((MediaWikiPage) pages.get(0));
						}
					}

					@Override
					public void onFailure(Exception e) {
						Log.e(Constants.getLogTag(this), "Error getting talk page", e);
					}
				}, tag);
			}
		}
	}
}
