/*
 * Copyright 2015 John Krause & Shelter Tech
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.network.service.mediawiki;

import android.util.Log;

import com.diginido.seeme.Constants;
import com.diginido.seeme.network.data.NetworkData;
import com.diginido.seeme.network.provider.MediaWikiApiProvider;
import com.diginido.seeme.network.request.mediawiki.MediaWikiApiRequest;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.Map;

/**
 * @author John Krause
 * @since 8/3/15
 */
public abstract class MediaWikiServiceBase {

	protected MediaWikiApiProvider mediaWikiApiProvider;

	protected MediaWikiApiRequest processQueryContinue(String response, MediaWikiApiRequest request) {
		MediaWikiApiRequest queryContinueRequest = null;
		Gson gson = NetworkData.getInstance().getGson();
		try {
			JsonObject queryContinue = gson.fromJson(response, JsonObject.class)
					.getAsJsonObject("query-continue");
			if (queryContinue != null) {
				for (Map.Entry<String, JsonElement> entry : queryContinue.entrySet()) {
					for (Map.Entry<String, JsonElement> elementEntry :
							entry.getValue().getAsJsonObject().entrySet()) {
						request.setQueryParam(elementEntry.getKey(), elementEntry.getValue().getAsString());
					}
				}
				queryContinueRequest = request;
			}
		} catch (ClassCastException e) {
			Log.e(Constants.getLogTag(this),
					String.format("Problem getting query continue from response %s", response), e);
		}
		return queryContinueRequest;
	}

	public void cancel(Object tag) {
		if (mediaWikiApiProvider != null) {
			mediaWikiApiProvider.cancelAllRequests(tag);
		}
	}
}
