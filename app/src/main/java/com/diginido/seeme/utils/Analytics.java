/*
 * Copyright 2015 John Krause & Shelter Tech
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.utils;

import com.diginido.seeme.Constants;
import com.diginido.seeme.SeeMeApplication;
import com.diginido.seeme.config.ABTests;
import com.diginido.seeme.model.Section;
import com.diginido.seeme.network.response.model.Page;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import ly.count.android.sdk.Countly;
import ly.count.android.sdk.DeviceId;
import ly.count.android.sdk.OpenUDIDAdapter;

/**
 * @author John Krause
 * @since 9/7/15
 */
public class Analytics {

	private static final String COUNTLY_SERVER = "https://cloud.count.ly/";
	private static final String COUNTLY_APP_KEY = "88cfd4b756e5c76bbb24a6abdf4b602d4a520630";

	public static final String HOME_SCREEN_ICONS_EVENT = "homeScreenIconSelected",
			SEARCH_BUTTON_PRESS_EVENT = "searchButtonPressed",
			MAP_SWITCHED_TO_EVENT = "mapSwitchedTo",
			LIST_SWITCHED_TO_EVENT = "listSwitchedTo",
			SECTION_SWITCHED_EVENT = "sectionSwitched",
			RATE_PAGE_TAPPED_EVENT = "ratePageTapped",
			FIRST_PAGE_VIEWED = "firstPageViewed",
			PAGE_VIEWED_EVENT = "pageViewed";

	public static void init() {
		if (!Constants.DEBUG) {
			Countly.sharedInstance()
					.init(SeeMeApplication.getAppContext(),
							COUNTLY_SERVER,
							COUNTLY_APP_KEY,
							null,
							DeviceId.Type.OPEN_UDID);
			Countly.sharedInstance().enableCrashReporting();
		} else {

		}
	}

	public static void onActivityStart() {
		if (!Constants.DEBUG)
			Countly.sharedInstance().onStart();
	}

	public static void onActivityStop() {
		if (!Constants.DEBUG)
			Countly.sharedInstance().onStop();
	}

	public static void recordABTestInitEvent(String event, Map<String, String> data) {
		recordEvent(event, data, 1);
	}

	public static void recordHomeScreenSectionSelectionEvent(Section section) {
		Map<String, String> data = new HashMap<>(1);
		data.put("section", section.getName());
		data.put("abTestOn", Boolean.toString(ABTests.isTestOn(ABTests.HOME_SCREEN_ICONS_TEST)));
		recordEvent(HOME_SCREEN_ICONS_EVENT, data, 1);
	}

	public static void recordSearchButtonTapped() {
		recordEvent(SEARCH_BUTTON_PRESS_EVENT);
	}

	public static void recordMapToggled(Section section, long currentSectionStartTimeMillis,
										boolean isMapShowingNow) {
		long totalTimeInSection = TimeUnit.MILLISECONDS
				.toSeconds(System.currentTimeMillis() - currentSectionStartTimeMillis);
		Map<String, String> data = new HashMap<>(2);
		data.put("section", section.getName());
		data.put("timeBeforeSwitchSeconds", Long.toString(totalTimeInSection));
		String event = isMapShowingNow ? LIST_SWITCHED_TO_EVENT : MAP_SWITCHED_TO_EVENT;
		recordEvent(event, data, 1);
	}

	public static void recordSectionSwitched(Section currentSection, Section sectionSwitchedTo) {
		Map<String, String> data = new HashMap<>(2);
		data.put("currentSection", currentSection.getName());
		data.put("sectionSwitchedTo", sectionSwitchedTo.getName());
		recordEvent(SECTION_SWITCHED_EVENT, data, 1);
	}

	public static void recordRatePageTapped(Page page) {
		Map<String, String> data = new HashMap<>(1);
		data.put("pageName", page.getTitle());
		recordEvent(RATE_PAGE_TAPPED_EVENT, data, 1);
	}

	public static void recordFirstPageViewed(Section section, Page page, long searchStartTime, boolean isMapShowing) {
		Map<String, String> data = new HashMap<>(1);
		data.put("section", section.getName());
		data.put("pageName", page.getTitle());
		data.put("isFromMap", Boolean.toString(isMapShowing));
		data.put("timeBeforeSelectionSeconds",
				Long.toString(TimeUnit.MILLISECONDS.
						toSeconds(System.currentTimeMillis() - searchStartTime)));
		recordEvent(FIRST_PAGE_VIEWED, data, 1);
		recordPageViewed(section, page, isMapShowing);
	}

	public static void recordPageViewed(Section section, Page page, boolean isMapShowing) {
		Map<String, String> data = new HashMap<>(1);
		data.put("section", section.getName());
		data.put("pageName", page.getTitle());
		data.put("isFromMap", Boolean.toString(isMapShowing));
		recordEvent(PAGE_VIEWED_EVENT, data, 1);
	}

	private static void recordEvent(String event) {
		recordEvent(event, null, 1);
	}

	private static void recordEvent(String event, Map<String, String> data, int count) {
		if (!Constants.DEBUG) {
			Countly.sharedInstance().recordEvent(event, data, count);
		}
	}

	public static String getOpenUDID() {
		if (!Constants.DEBUG
				&& OpenUDIDAdapter.isInitialized() && OpenUDIDAdapter.isOpenUDIDAvailable()) {
			return OpenUDIDAdapter.getOpenUDID();
		} else {
			return "debugbuildid";
		}
	}
}
