/*
 * Copyright 2015 John Krause
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.diginido.legacy.appseeme.R;


/**
 * @author John Krause
 * @since 9/30/15
 */
public class LocationHelper {

	private static final String DONT_SHOW_AGAIN_PREF = "dontShowAgain";

	Context context;
	private boolean dontShowAgain;
	private SharedPreferences sharedPreferences;

	public LocationHelper(Context context) {
		this.context = context;
		sharedPreferences = context.getSharedPreferences(LocationHelper.class.getName(), Context.MODE_PRIVATE);
		dontShowAgain = sharedPreferences.getBoolean(DONT_SHOW_AGAIN_PREF, false);
	}

	public void promptIfLocationNotAvailable() {
		if (!isLocationServiceOn() && !dontShowAgain) {
			showLocationPromptDialog();
		}
	}

	public boolean isLocationServiceOn() {
		LocationManager service = (LocationManager)
				context.getSystemService(Context.LOCATION_SERVICE);
		return service.isProviderEnabled(LocationManager.GPS_PROVIDER)
				|| service.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
	}

	public void showLocationPromptDialog() {
		final View dontShowAgainCheck = View.inflate(context, R.layout.dialog_dont_show_check, null);
		CheckBox checkBox = (CheckBox) dontShowAgainCheck.findViewById(R.id.checkbox);
		checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				dontShowAgain = isChecked;
			}
		});
		final AlertDialog alertDialog = new AlertDialog.Builder(context)
				.setCancelable(true)
				.setTitle(R.string.location_services_off_dialog_title)
				.setMessage(R.string.location_services_off_dialog_content)
				.setPositiveButton(R.string.location_services_off_dialog_confirm, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						openLocationSettings();
						updatePreferences();
					}
				})
				.setNegativeButton(R.string.location_services_off_dialog_deny, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						updatePreferences();
					}
				})
				.setView(dontShowAgainCheck)
				.create();
		alertDialog.show();
	}

	public void openLocationSettings() {
		Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		context.startActivity(intent);
	}

	private void updatePreferences() {
		sharedPreferences.edit().putBoolean(DONT_SHOW_AGAIN_PREF, dontShowAgain).apply();
	}

}
