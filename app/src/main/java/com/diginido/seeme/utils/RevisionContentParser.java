/*
 * Copyright 2015 John Krause & Shelter Tech
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.diginido.seeme.utils;

import android.util.Log;

import com.diginido.seeme.model.PageContent;
import com.diginido.seeme.network.response.model.mediawiki.MediaWikiPage;
import com.diginido.seeme.network.response.model.mediawiki.Revisions;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * @author John Krause
 * @since 8/4/15
 */
public class RevisionContentParser {

	private static RevisionContentParser instance;

	private SAXParser saxParser;

	public PageContent parsePage(MediaWikiPage page) {
		PageContent pageContent = new PageContent();
		SAXParser saxParser = getParser();
		InputSource contentSource = getXmlInputSource(page);
		DefaultHandler handler = getHandler(pageContent);
		if (saxParser != null && contentSource != null) {
			try {
				XMLReader xmlReader = saxParser.getXMLReader();
				xmlReader.setContentHandler(handler);
				xmlReader.parse(contentSource);
			} catch (SAXException | IOException e) {
				Log.e(RevisionContentParser.class.getName(), "Error parsing page content", e);
			}
		}
		return pageContent;
	}

	private DefaultHandler getHandler(PageContent pageContent) {
		return new RevisionContentHandler(pageContent);
	}

	private InputSource getXmlInputSource(MediaWikiPage page) {
		InputSource inputSource = null;
		Revisions[] revisions = page.getRevisions();
		if (revisions != null && revisions.length > 0) {
			Revisions revision = revisions[0];
			String parseTree = revision.getParsetree();
			if (parseTree != null) {
				inputSource = new InputSource(new StringReader(parseTree));
			}
		}
		return inputSource;
	}

	private SAXParser getParser() {
		if (saxParser == null) {
			SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
			try {
				saxParser = saxParserFactory.newSAXParser();
			} catch (ParserConfigurationException | SAXException e) {
				Log.e(RevisionContentParser.class.getName(), "Error getting SAX parser", e);
			}
		}
		return saxParser;
	}

	public static RevisionContentParser getInstance() {
		if (instance == null) {
			instance = new RevisionContentParser();
		}
		return instance;
	}

	private class RevisionContentHandler extends DefaultHandler {

		private static final int VALUE_BUILDER_SIZE = 1000;

		private PageContent pageContent;

		private String lastNameTagValue;
		private StringBuilder valueBuilder = new StringBuilder(VALUE_BUILDER_SIZE);

		public RevisionContentHandler(PageContent pageContent) {
			super();
			this.pageContent = pageContent;
		}

		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
			valueBuilder.setLength(0);
		}

		@Override
		public void characters(char[] ch, int start, int length) throws SAXException {
			// handle data within tags here
			valueBuilder.append(ch, start, length);
		}

		@Override
		public void endElement(String uri, String localName, String qName) throws SAXException {
			// add data to page content here for each tag processed
			switch (qName) {
				case "name":
					lastNameTagValue = valueBuilder.toString().trim();
					break;
				case "value":
					if (lastNameTagValue != null) {
						processNameValuePair(lastNameTagValue, valueBuilder.toString().trim());
						lastNameTagValue = null;
					}
					break;
				case "root":
					processNameValuePair(qName, valueBuilder.toString().trim());
					break;
				default:
					break;
			}
		}

		private void processNameValuePair(String name, String value) {
			switch (name) {
				case "Address":
					pageContent.setAddress(value);
					break;
				case "Phone":
					pageContent.setPhone(value.replaceAll("\\s", ""));
					break;
				case "Hours":
					pageContent.setHours(value);
					break;
				case "Language(s)":
					pageContent.setLanguages(value);
					break;
				case "Contact(s)":
					pageContent.setContacts(value);
					break;
				case "SummaryText":
					pageContent.setSummary(value);
					break;
				case "Website":
					pageContent.setWebsite(value);
					break;
				case "root":
					pageContent.setContent(processContent(value));
					break;
				default:
					break;
			}
		}

		private String processContent(String content) {
			return content.replaceAll("\\[\\[[^\\]]*\\]\\]", "")
					.replaceAll("\\n+", "\n");
		}
	}
}
